import { useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import 'react-native-gesture-handler';
import { QueryProvider } from 'shared/core/infrastructure/libs/tanstack-query';
import { initDependencies } from 'shared/dependencies';
import { AppNavigation } from 'shared/navigation/app-navigation';

function App(): JSX.Element {
   useEffect(() => {
      initDependencies();
   }, []);

   return (
      <SafeAreaView style={{ flex: 1 }}>
         <QueryProvider>
            <AppNavigation />
         </QueryProvider>
      </SafeAreaView>
   );
}

export default App;
