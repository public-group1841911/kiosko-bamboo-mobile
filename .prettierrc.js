module.exports = {
   tabWidth: 3,
   arrowParens: 'avoid',
   bracketSameLine: true,
   bracketSpacing: true,
   singleQuote: true,
   trailingComma: 'none',
   jsxSingleQuote: true,
   printWidth: 120
};
