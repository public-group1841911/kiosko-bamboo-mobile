module.exports = {
   root: true,
   extends: ['@react-native', 'prettier'],
   parser: '@typescript-eslint/parser',
   rules: {
      'prettier/prettier': ['off', { singleQuote: true }],
      eqeqeq: ['error'],
      'react/react-in-jsx-scope': 'off',
      'no-empty-function': 'error',
      'no-implicit-coercion': 'error',
      'no-multiple-empty-lines': 2,
      'react-native/no-inline-styles': 'off',
      '@typescript-eslint/no-var-requires': 'off',
      '@typescript-eslint/no-unused-vars': ['warn'],
      '@typescript-eslint/no-explicit-any': ['warn'],
      '@typescript-eslint/no-duplicate-enum-values': ['warn']
   }
};
