module.exports = {
   presets: ['module:@react-native/babel-preset'],
   plugins: [
      [
         'module-resolver',
         {
            extensions: [
               '.ios.js',
               '.android.js',
               '.js',
               '.ts',
               '.tsx',
               '.json',
            ],
            root: ['./src'],
            alias: {
               screens: ['./src/screens/index'],
               components: ['./src/shared/components'],
               shared: ['./src/shared'],
            },
         },
      ],
   ],
};
