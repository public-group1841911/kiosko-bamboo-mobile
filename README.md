# Kiosko Bamboo Mobile

**General criteria for the application development:**

## Tech Stack

- React Native CLI - Typescript
- React Navigation
- Zustand
- Graphql
- convict
- awilix
- React query
- FlashList
- RN Reanimated

## Architecture

- Hexagonal Architecture
- Driven Domain Design

## Captures
![signIn](img-1.png)
![signUp](img-2.png)
![recovery](img-3.png)
![recovery](img-4.png)