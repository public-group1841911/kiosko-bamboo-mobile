import { create } from 'zustand';

type Auth = { userId: string; token: string; isAuthenticated: boolean };

type AuthStore = {
   auth: Auth;
   setAuth(auth: Auth): void;
   clearAuth(): void;
};

export const useAuthStore = create<AuthStore>(() => ({
   auth: { userId: '123', token: 'xxx', isAuthenticated: false },
   setAuth: () => {},
   clearAuth: () => {}
}));
