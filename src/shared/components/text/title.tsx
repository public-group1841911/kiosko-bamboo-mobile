import { FC, PropsWithChildren } from 'react';
import { StyleSheet, Text, TextStyle } from 'react-native';
import { titleTheme } from 'shared/styles/components-theme';

import { AppTheme } from 'shared/styles/theme';

interface StyleProps {
   testID?: string;
   style?: TextStyle;
   theme?: AppTheme;
   level?: 'h2' | 'h3';
   align?: 'center' | 'left' | 'right';
}

type TitleProps = StyleProps & PropsWithChildren;

export const Title: FC<TitleProps> = props => {
   const { children, testID, level = 'h1', theme = 'dark', align, style } = props;

   const themeContext = titleTheme[theme];

   return (
      <Text
         testID={testID}
         style={{
            ...styles[level],
            ...themeContext.text,
            ...style,
            textAlign: align,
         }}
      >
         {children}
      </Text>
   );
};

const styles = StyleSheet.create({
   h1: {
      fontSize: 26,
      fontWeight: '600',
      paddingVertical: 10,
      marginBottom: 5
   },
   h2: {
      fontSize: 19,
      fontWeight: '600',
      paddingVertical: 5,
   },
   h3: {
      fontSize: 16,
      fontWeight: '600',
      paddingVertical: 5,
   },
});
