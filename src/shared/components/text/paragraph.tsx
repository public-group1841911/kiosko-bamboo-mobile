import { FC, PropsWithChildren } from 'react';
import { StyleSheet, Text, TextStyle } from 'react-native';
import { paragraphTheme } from 'shared/styles/components-theme';
import { AppTheme } from 'shared/styles/theme';

interface ParagraphProps {
   testID?: string;
   style?: TextStyle | TextStyle[];
   theme?: AppTheme;
   align?: 'center' | 'left' | 'right';
   size?: 'small' | 'normal' | 'large';
   weight?: '200' | '400' | '500' | '800';
}

type Paragraph = ParagraphProps & PropsWithChildren;

export const Paragraph: FC<Paragraph> = props => {
   const { children, testID, size, weight, theme = 'dark', style } = props;
   const themeContext = paragraphTheme[theme];

   return (
      <Text
         testID={testID}
         style={{
            ...styles.paragraph,
            ...themeContext.text,
            ...styles[size ?? 'normal'],
            fontWeight: weight ?? '300',
            ...style,
         }}
      >
         {children}
      </Text>
   );
};

const styles = StyleSheet.create({
   paragraph: {
      fontSize: 18,
      fontWeight: '300',
      paddingVertical: 5,
   },
   small: {
      fontSize: 13,
   },
   normal: {
      fontSize: 16,
   },
   large: {
      fontSize: 18,
   },
});
