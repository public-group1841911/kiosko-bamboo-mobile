import { StyleSheet } from "react-native";
import { theme } from "shared/styles/theme";

export const styles = StyleSheet.create({
   logoContainer: {
      alignSelf: 'center',
      gap: 10,
      marginBottom: 10,
   },
   logo: {
      height: 80,
      width: 70,
      alignSelf: 'center',
      resizeMode: 'contain',
   },
   logoText: {
      fontWeight: '800',
      fontSize: 26,
      color: theme.colors.tertiary,
   },
});
