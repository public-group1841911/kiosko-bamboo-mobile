import { Image, Text, View } from 'react-native';
import { styles } from './logo-styles';
const logo = require('shared/assets/branding/logo.png');

export const Logo = () => {
   return (
      <View style={styles.logoContainer}>
         <Image style={styles.logo} source={logo} />
         <Text style={styles.logoText}>Kiosko Bamboo</Text>
      </View>
   );
};
