import { FC, PropsWithChildren } from 'react';
import { View, ViewStyle } from 'react-native';
import { theme } from 'shared/styles/theme';

const { boxShadow } = theme;

interface StyleProps extends ViewStyle {
   testID?: string;
   boxShadow?: boolean;
   
}

type BoxProps = StyleProps & PropsWithChildren;

export const Box: FC<BoxProps> = props => {
   const { children, testID, ...style } = props;

   return (
      <View
         testID={testID}
         style={{
            backgroundColor: '#d4d4d4',
            ...(props.boxShadow && { ...boxShadow }),
            ...style,
         }}
      >
         {children}
      </View>
   );
};
