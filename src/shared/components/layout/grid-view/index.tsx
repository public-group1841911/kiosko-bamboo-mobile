import { ReactNode } from 'react';
import { StyleSheet, View } from 'react-native';
type GridViewProps<T> = {
   data: T[];
   renderItem(props: T): ReactNode;
   col?: number;
   colGap?: number;
   rowGap?: number;
};
export const GridView = <T extends unknown>({
   data,
   renderItem,
   col = 2,
   colGap = 5,
   rowGap = 5
}: GridViewProps<T>) => {
   return (
      <View style={styles.container}>
         {data.map(item => {
            return (
               <View key={Math.random()} style={{ width: `${100 / col}%`, }}>
                   <View style={{paddingBottom: rowGap, paddingHorizontal: colGap}}>{renderItem(item)}</View>
               </View>
            );
         })}
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      width: '100%',
      flexDirection: 'row',
      flexWrap: 'wrap',
   },
});
