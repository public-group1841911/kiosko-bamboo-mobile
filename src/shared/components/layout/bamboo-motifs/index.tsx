import { Image, StyleSheet } from 'react-native';
const bamboos = require('shared/assets/branding/bamboos-motifs.png');
export const BambooMotifs = () => {
   return (
      <>
         <Image style={styles.bambooMotifL} source={bamboos} />
         <Image style={styles.bambooMotifR} source={bamboos} />
         <Image style={styles.bambooMotifB} source={bamboos} />
      </>
   );
};

const styles = StyleSheet.create({
   bambooMotifR: {
      position: 'absolute',
      height: 650,
      width: 650,
      left: 130,
      bottom: 520,
      zIndex: -1,
   },
   bambooMotifL: {
      position: 'absolute',
      right: 10,
      bottom: 450,
      zIndex: -1,
   },
   bambooMotifB: {
      position: 'absolute',
      right: 160,
      bottom: -300,
      zIndex: -1,
   },
});
