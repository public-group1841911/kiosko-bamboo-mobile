import { FC } from 'react';
import { View, ViewStyle } from 'react-native';
import { theme } from 'shared/styles/theme';

type Divider = {
   style?: ViewStyle;
   color?: 'tertiary' | 'onTertiary' | 'secondary';
   direction?: 'vertical' | 'horizontal';
   marginHorizontal?: number;
   marginVertical?: number;
};

export const Divider: FC<Divider> = ({
   color,
   direction,
   marginHorizontal,
   marginVertical,
   style,
}) => {
   return (
      <View
         style={{
            borderWidth: .4,
            borderBottomColor: theme.colors[color ?? 'tertiary'],
            transform: [{ rotate: direction === 'vertical' ? '180deg' : '0deg' }],
            marginHorizontal,
            marginVertical,
            ...style,
         }}
      />
   );
};
