import { FC, PropsWithChildren } from 'react';
import { View, ViewStyle } from 'react-native';
import { layout } from 'shared/styles/layout';
const { screen } = layout;

interface StyleProps extends ViewStyle {
   testID?: string;
}

type LayoutProps = StyleProps & PropsWithChildren;
export const Layout: FC<LayoutProps> = props => {
   const { children, ...style } = props;
   return <View style={{ ...screen, ...style }}>{children}</View>;
};
