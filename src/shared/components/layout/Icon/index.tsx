import { FC } from 'react';
import { Image, ImageSourcePropType, ImageStyle, StyleProp } from 'react-native';

type Icon = {
   style?: StyleProp<ImageStyle>
   source: ImageSourcePropType;
   size?: number;
};

export const Icon: FC<Icon> = ({ source, size, style }) => {
   return (
      <Image
         style={[{ height: size ?? 20, width: size ?? 20 }, style]}
         resizeMode='contain'
         source={source}
      />
   );
};
