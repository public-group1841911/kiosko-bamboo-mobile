import { FC } from 'react';
import { Image, ImageSourcePropType, Platform, StyleSheet, Text, TextInput, TextInputProps, View } from 'react-native';
import { useInputController } from 'shared/hooks/use-form';
import { FormControl } from 'shared/hooks/use-form/input-types';
import { textInputTheme } from 'shared/styles/components-theme';
import { metrics } from 'shared/styles/metrics';
import { AppTheme, theme } from 'shared/styles/theme';
const { colors } = theme;

interface InputProps {
   name: string;
   control: FormControl
   testID?: string;
   label?: string;
   secure?: boolean;
   ico?: ImageSourcePropType;
   sizeIco?: number;
   invertIco?: boolean;
   theme?: AppTheme;
}

export const TextField: FC<InputProps & TextInputProps> = props => {
   const {control, label, name, secure, ico, sizeIco, invertIco, theme = 'dark' } = props;
   
   const { field, fieldState } = useInputController({ name, control, defaultValue: '' });
   const styleByPlatform = Platform.OS === 'ios' ? styles.textInputIOS : styles.textInputAND;
   const themeContext = textInputTheme[theme];

   return (
      <View style={styles.container}>
         {label && <Text style={[styles.label, themeContext.text, { paddingLeft: ico && 45 }]}>{label}</Text>}
         <View style={styles.inputContainer}>
            {ico && (
               <View style={[styles.icoContainer, themeContext.ico]}>
                  <Image
                     style={{
                        width: sizeIco ?? 18,
                        height: sizeIco ?? 18,
                        transform: [{ scaleX: invertIco ? -1 : 1 }]
                     }}
                     source={ico ? ico : {}}
                  />
               </View>
            )}

            <TextInput
               {...props}
               style={[styles.input, themeContext.input, styleByPlatform, { paddingLeft: ico && 8 }]}
               placeholderTextColor={themeContext.placeholderColor}
               cursorColor={themeContext.caretColor}
               secureTextEntry={secure}
               onChangeText={field.onChange}
               onBlur={field.onBlur}
               value={field.value}
            />
         </View>
         {fieldState.error && <Text style={styles.error}>{fieldState.error.message}</Text>}
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      marginTop: 10,
      marginBottom: 20
   },
   inputContainer: {
      flexDirection: 'row'
   },
   icoContainer: {
      width: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRightWidth: 1,
      borderBottomWidth: 1
   },
   input: {
      flex: 1,
      height: metrics.inputHeight,
      borderBottomWidth: 1,
      padding: 0
   },
   textInputAND: {
      fontSize: 20
   },
   textInputIOS: {
      fontSize: 20,
      borderBottomWidth: 2
   },
   label: {
      paddingLeft: 0,
      fontSize: 13
   },
   error: {
      fontSize: 16,
      position: 'absolute',
      bottom: -23,
      color: colors.error
   }
});
