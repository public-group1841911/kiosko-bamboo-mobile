import { FC } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import { useInputController } from 'shared/hooks/use-form';
import { FormControl } from 'shared/hooks/use-form/input-types';
import { textInputTheme } from 'shared/styles/components-theme';
import { metrics } from 'shared/styles/metrics';
import { AppTheme, theme } from 'shared/styles/theme';
const Arrow = require('shared/assets/forms/selector-arrow.png');
const { colors } = theme;

interface InputProps {
   control: FormControl;
   testID?: string;
   label?: string;
   data: {}[];
   sizeIco?: number;
   invertIco?: boolean;
   theme?: AppTheme;
   labelField: string;
   valueField: string;
   placeholder: string;
   name: string;
}

export const SelectField: FC<InputProps> = ({
   control,
   label,
   placeholder,
   name,
   data,
   labelField,
   valueField,
   theme = 'dark'
}) => {
   const { field, fieldState } = useInputController({ name, control, defaultValue: null });

   const themeContext = textInputTheme[theme];

   return (
      <View style={styles.container}>
         {label && <Text style={[styles.label, themeContext.text]}>{label}</Text>}
         <Dropdown
            style={[styles.input, themeContext.input]}
            placeholderStyle={{
               color: themeContext.placeholderColor,
               fontSize: 20
            }}
            containerStyle={{
               backgroundColor: colors.primary
            }}
            selectedTextStyle={{ color: colors.tertiary, fontSize: 18 }}
            itemTextStyle={{ color: colors.tertiary, fontSize: 18 }}
            renderRightIcon={() => <Image style={{ height: 18, width: 18 }} source={Arrow} />}
            activeColor='#ffddb433'
            labelField={labelField as never}
            valueField={valueField as never}
            placeholder={placeholder}
            data={data}
            value={field.value}
            onBlur={field.onBlur}
            onChange={field.onChange}
         />
         {fieldState.error && <Text style={styles.error}>{fieldState.error.message}</Text>}
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      marginTop: 10,
      marginBottom: 20
   },
   label: {
      fontSize: 13
   },
   input: {
      height: metrics.inputHeight,
      borderBottomWidth: 1
   },
   error: {
      fontSize: 16,
      position: 'absolute',
      bottom: -23,
      color: colors.error
   }
});
