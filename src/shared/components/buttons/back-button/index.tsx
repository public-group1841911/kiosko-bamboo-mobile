import { FC } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { theme } from 'shared/styles/theme';
const ArrowBackLight = require('shared/assets/navigation/arrow-back-light.png');

type BackButton = {
   goBack(): void;
};
export const BackButton: FC<BackButton> = ({ goBack }) => {
   return (
      <TouchableOpacity style={styles.button} onPress={goBack}>
         <Image style={styles.arrowImg} source={ArrowBackLight} />
         <Text style={styles.buttonText}>Volver</Text>
      </TouchableOpacity>
   );
};

const styles = StyleSheet.create({
   button: {
      padding: 10,
      flexDirection: 'row',
      alignItems: 'center',
      top: -40, 
   },
   buttonText: {
      fontSize: 16,
      fontWeight: '600',
      color: theme.colors.backdrop
   },
   arrowImg: {
      height: 20,
      width: 20,
      opacity: 0.6
   }
});
