import { FC } from 'react';
import { StyleSheet, Text, TouchableOpacity, ViewStyle } from 'react-native';
import { buttonLeafTheme } from 'shared/styles/components-theme';
import { AppTheme, theme } from 'shared/styles/theme';
const { boxShadow } = theme;

type LeafButtonProps = {
   style?: ViewStyle;
   text: string;
   theme?: AppTheme | 'primary' | 'error';
   onPress(): void;
};
export const LeafButton: FC<LeafButtonProps> = ({
   onPress,
   text,
   theme = 'dark',
   style,
}) => {
   const themeContext = buttonLeafTheme[theme];

   return (
      <TouchableOpacity
         activeOpacity={0.8}
         style={[styles.button, themeContext.background, { ...style }]}
         onPress={onPress}
      >
         <Text style={[styles.text, themeContext.text]}>{text}</Text>
      </TouchableOpacity>
   );
};

const styles = StyleSheet.create({
   button: {
      alignSelf: 'center',
      alignItems: 'center',
      marginVertical: 10,
      padding: 14,
      width: '80%',
      borderTopLeftRadius: 20,
      borderBottomRightRadius: 20,
      ...boxShadow,
   },
   text: {
      fontSize: 18,
      fontWeight: '500',
   },
});
