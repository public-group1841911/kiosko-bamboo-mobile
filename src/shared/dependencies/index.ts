import { asClass, createContainer } from 'awilix';
import { HttpAxiosClient } from 'shared/core/infrastructure/http';
import { GetAllQuestions } from '../../screens/sign-up-screen/core/application/get-all-questions';
import { AccountCreator } from '../../screens/sign-up-screen/core/application/account-creator';
import { SignUpAxiosService } from '../../screens/sign-up-screen/core/infrastructure/services/sign-up-axios-service';
import { SignUpAxiosSpecialService } from '../../screens/sign-up-screen/core/infrastructure/services/sign-up-axios-special-service';
import { SignUpAxiosRepository } from '../../screens/sign-up-screen/core/infrastructure/sign-up-axios-repository';
import { SignUpAxiosSpecialServiceRepository } from '../../screens/sign-up-screen/core/infrastructure/sign-up-axios-special-service-repository';

const container = createContainer();

export function initDependencies() {
   container.register({
      httpAxiosService: asClass(HttpAxiosClient),
      /** User account **/
      signUpAxiosService: asClass(SignUpAxiosService),
      signUpAxiosSpecialService: asClass(SignUpAxiosSpecialService),
      signUpAxiosSpecialServiceRepository: asClass(SignUpAxiosSpecialServiceRepository),
      signUpRepository: asClass(SignUpAxiosRepository),
      accountCreator: asClass(AccountCreator),
      getAllQuestions: asClass(GetAllQuestions)
   });
   container.resolve('httpAxiosService');
   container.resolve('signUpAxiosService');
   container.resolve('signUpAxiosSpecialService');
   container.resolve('signUpRepository');
   container.resolve('signUpAxiosSpecialServiceRepository');
}

export { container };
