import { ViewStyle } from 'react-native';
import { palette } from '../theme';

export const buttonLeafTheme = {
   light: {
      text: <ViewStyle>{
         color: palette.onTertiaryContainer,
      },
      background: <ViewStyle>{
         backgroundColor: palette.tertiary,
      },
   },
   dark: {
      text: <ViewStyle>{
         color: palette.tertiary,
      },
      background: <ViewStyle>{
         backgroundColor: palette.onTertiaryContainer,
      },
   },
   primary: {
      text: <ViewStyle>{
         color: palette.surface,
      },
      background: <ViewStyle>{
         backgroundColor: palette.primary,
      },
   },
   error: {
      text: <ViewStyle>{
         color: palette.surface,
      },
      background: <ViewStyle>{
         backgroundColor: palette.error,
      },
   },
};
