import { ViewStyle } from 'react-native';
import { palette } from '../theme';

export const textInputTheme = {
   light: {
      text: <ViewStyle>{
         color: palette.tertiary,
      },
      input: <ViewStyle>{
         color: palette.tertiary,
         borderBottomColor: palette.tertiary,
         backgroundColor: palette.primary
      },
      ico: <ViewStyle>{
         borderRightColor: palette.tertiary,
         borderBottomColor: palette.tertiary,
      },
      placeholderColor: '#ffffff44',
      caretColor: palette.tertiary,
   },
   dark: {
      text: <ViewStyle>{
         color: palette.onTertiaryContainer,
      },
      input: <ViewStyle>{
         color: palette.onTertiaryContainer,
         borderBottomColor: palette.onTertiaryContainer,
      },
      ico: <ViewStyle>{
         borderRightColor: palette.onTertiaryContainer,
         borderBottomColor: palette.onTertiaryContainer,
      },
      placeholderColor: '#ffffff44',
      caretColor: palette.onTertiaryContainer,
   },
};
