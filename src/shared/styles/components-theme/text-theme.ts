import { ViewStyle } from "react-native";
import { palette } from "../theme";

export const titleTheme = {
   light: {
      text: <ViewStyle>{
         color: palette.onTertiaryContainer,
      },
      background: <ViewStyle>{
         backgroundColor: palette.tertiary,
      },
   },
   dark: {
      text: <ViewStyle>{
         color: palette.tertiary,
      },
      background: <ViewStyle>{
         backgroundColor: palette.onTertiaryContainer,
      },
   },
};

export const paragraphTheme = {
   light: {
      text: <ViewStyle>{
         color: palette.onTertiaryContainer,
      },
      background: <ViewStyle>{
         backgroundColor: palette.tertiary,
      },
   },
   dark: {
      text: <ViewStyle>{
         color: palette.tertiary,
      },
      background: <ViewStyle>{
         backgroundColor: palette.onTertiaryContainer,
      },
   },
};
