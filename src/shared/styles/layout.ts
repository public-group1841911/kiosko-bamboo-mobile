import { ViewStyle } from 'react-native';
import { theme } from './theme';
const { colors } = theme;

export const layout = {
   screen: {
      flex: 1,
      backgroundColor: colors.background,
   },
   paddingH: <ViewStyle>{
      paddingHorizontal: 10,
   },
};
