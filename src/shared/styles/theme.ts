import { ViewStyle } from 'react-native';
export const palette = {
   primary: '#003735',
   secondary: '#54686e',
   tertiary: '#ffddb4',

   primaryContainer: '#adecff',
   secondaryContainer: '#cce8e5',
   ternaryContainer: '#ffddb4',

   onTertiaryContainer: '#291800',

   neutral: '#E7E7E7',
   surface: '#fbfcfd',

   error: '#9e3e3e',
   success: '#198754',
   backdrop: '#c7c7c799',
   transparent: 'transparent',
};

export const theme = {
   boxShadow: <ViewStyle>{
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
   },

   colors: {
      background: palette.neutral,
      surface: palette.surface,
      primary: palette.primary,
      secondary: palette.secondary,
      tertiary: palette.tertiary,
      onTertiary: palette.onTertiaryContainer,
      error: palette.error,
      backdrop: palette.backdrop,
      transparent: palette.transparent,
   },
};

export type AppTheme = 'light' | 'dark';
