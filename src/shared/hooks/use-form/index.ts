import { zodResolver } from '@hookform/resolvers/zod';
import { FieldValues, UseControllerProps, UseFormProps, useController, useForm as useHookForm } from 'react-hook-form';

export function useForm<T extends FieldValues>(props?: UseFormProps<T>) {
   return useHookForm(props);
}

export function useInputController(props: UseControllerProps<FieldValues, string>) {
   return useController(props);
}

export const formResolver = zodResolver;