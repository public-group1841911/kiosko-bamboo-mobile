import { QueryFunction, QueryKey, UseQueryOptions, useQuery } from '@tanstack/react-query';

export function useFetch<T, Error>(
   key: string,
   func: QueryFunction<T, QueryKey, never> | undefined,
   options?: UseQueryOptions<T, Error, T, QueryKey>
) {
   return useQuery<T, Error>({ queryKey: [key], queryFn: func, ...options });
}
