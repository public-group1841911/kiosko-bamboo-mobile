export enum PUBLIC_SCREENS {
   /** Public navigation **/
   SignInScreen = 'SignInScreen',
   SignUpScreen = 'SignUpScreen',
   AccountRecoveryScreen = 'AccountRecoveryScreen',
}

export enum PRIVATE_SCREENS {
   Home = 'Home',
   CategoriesRoot = 'CategoriesRoot',
   CategoriesScreen = 'CategoriesScreen',
   OrdersScreen = 'OrdersScreen',
   ProductsScreen = 'ProductsScreen',
   FavoritesScreen = 'FavoritesScreen',
   ShoppingCartScreen = 'ShoppingCartScreen',
}
