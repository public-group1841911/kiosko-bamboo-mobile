import { NativeStackNavigationOptions, createNativeStackNavigator } from '@react-navigation/native-stack';
import { AccountRecoveryScreen, SignInScreen, SignUpAccountScreen } from 'screens/';
import { PUBLIC_SCREENS } from '../screen-names';
import { PublicNavigationParamList } from '../types';


const screenOpts:NativeStackNavigationOptions = {
   headerShown: false,
   contentStyle: { backgroundColor: 'transparent' },
};

const Stack = createNativeStackNavigator<PublicNavigationParamList>();

export function PublicScreensNavigation() {


   return (
      <Stack.Navigator screenOptions={screenOpts} >
         <Stack.Screen
            name={PUBLIC_SCREENS.SignInScreen}
            component={SignInScreen}
         />
         <Stack.Screen
            name={PUBLIC_SCREENS.SignUpScreen}
            component={SignUpAccountScreen}
         />
         <Stack.Screen
            name={PUBLIC_SCREENS.AccountRecoveryScreen}
            component={AccountRecoveryScreen}
         />
         </Stack.Navigator>
     
   );
}
