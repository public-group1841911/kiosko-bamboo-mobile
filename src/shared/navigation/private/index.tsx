import { createDrawerNavigator } from '@react-navigation/drawer';
import { DrawerNavigationOptions } from '@react-navigation/drawer/lib/typescript/src/types';
import {
   NativeStackNavigationOptions,
   createNativeStackNavigator,
} from '@react-navigation/native-stack';
import {
   CategoriesScreen,
   FavoritesScreen,
   HeaderShoppingCartButton,
   HomeScreen,
   OrdersScreen,
   ProductsScreen,
   ShoppingCartScreen,
} from 'screens/';
import { Icon } from 'shared/components/layout';
import { Paragraph } from 'shared/components/text/paragraph';
import { theme } from 'shared/styles/theme';
import { PRIVATE_SCREENS } from '../screen-names';
import { PrivateNavigationParamList } from '../types';
import { CustomDrawer } from './custom-drawer';
const HomeActiveIco = require('shared/assets/navigation/home-active.png');
const HomeInactiveIco = require('shared/assets/navigation/home-inactive.png');
const CategoriesActiveIco = require('shared/assets/navigation/categories-active.png');
const CategoriesInactiveIco = require('shared/assets/navigation/categories-inactive.png');
const OrdersActiveIco = require('shared/assets/navigation/orders-active.png');
const OrdersInactiveIco = require('shared/assets/navigation/orders-inactive.png');
const FavoritesActiveIco = require('shared/assets/navigation/favorites-active.png');
const FavoritesInactiveIco = require('shared/assets/navigation/favorites-inactive.png');
const Drawer = createDrawerNavigator<PrivateNavigationParamList>();
const Stack = createNativeStackNavigator();

const screenOpts: DrawerNavigationOptions = {
   headerTitle: '',
   headerTransparent: true,
   headerRight: () => <HeaderShoppingCartButton />,
   headerTintColor: theme.colors.tertiary,
   headerBackgroundContainerStyle: {
      backgroundColor: theme.colors.primary,
   },
   headerRightContainerStyle: {
      paddingRight: 15,
   },
   drawerActiveBackgroundColor: theme.colors.tertiary,
   drawerActiveTintColor: theme.colors.onTertiary,
   drawerStyle: {
      backgroundColor: theme.colors.primary,
   },
};

const categoriesRootOpts: NativeStackNavigationOptions = {
   headerShown: false,
   contentStyle: { backgroundColor: 'transparent' },
};

function CategoriesRoot() {
   return (
      <Stack.Navigator screenOptions={categoriesRootOpts}>
         <Stack.Screen
            name={PRIVATE_SCREENS.CategoriesScreen}
            component={CategoriesScreen}
         />
         <Stack.Screen
            name={PRIVATE_SCREENS.ProductsScreen}
            component={ProductsScreen}
         />
         <Stack.Screen
            name={PRIVATE_SCREENS.ShoppingCartScreen}
            component={ShoppingCartScreen}
         />
      </Stack.Navigator>
   );
}

export function PrivateScreenNavigation() {
   return (
      <Drawer.Navigator
         initialRouteName='CategoriesRoot'
         drawerContent={props => <CustomDrawer {...props} />}
         screenOptions={screenOpts}
      >
         <Drawer.Screen
            name={PRIVATE_SCREENS.Home}
            component={HomeScreen}
            options={{
               drawerIcon: ({ focused }) => (
                  <Icon
                     source={focused ? HomeActiveIco : HomeInactiveIco}
                     size={22}
                  />
               ),
               drawerLabel: ({ focused }) => (
                  <Paragraph
                     theme={focused ? 'light' : 'dark'}
                     size='large'
                     weight={focused ? '500' : undefined}
                  >
                     Inicio
                  </Paragraph>
               ),
            }}
         />
         <Drawer.Screen
            name={PRIVATE_SCREENS.CategoriesRoot}
            component={CategoriesRoot}
            options={{
               drawerIcon: ({ focused }) => (
                  <Icon
                     source={
                        focused ? CategoriesActiveIco : CategoriesInactiveIco
                     }
                     size={22}
                  />
               ),
               drawerLabel: ({ focused }) => (
                  <Paragraph
                     theme={focused ? 'light' : 'dark'}
                     size='large'
                     weight={focused ? '500' : undefined}
                  >
                     Categorías
                  </Paragraph>
               ),
            }}
         />
         <Drawer.Screen
            name={PRIVATE_SCREENS.OrdersScreen}
            component={OrdersScreen}
            options={{
               drawerIcon: ({ focused }) => (
                  <Icon
                     source={focused ? OrdersActiveIco : OrdersInactiveIco}
                     size={24}
                  />
               ),
               drawerLabel: ({ focused }) => (
                  <Paragraph
                     theme={focused ? 'light' : 'dark'}
                     size='large'
                     weight={focused ? '500' : undefined}
                  >
                     Pedidos
                  </Paragraph>
               ),
            }}
         />
         <Drawer.Screen
            name={PRIVATE_SCREENS.FavoritesScreen}
            component={FavoritesScreen}
            options={{
               drawerIcon: ({ focused }) => (
                  <Icon
                     source={
                        focused ? FavoritesActiveIco : FavoritesInactiveIco
                     }
                     size={23}
                  />
               ),
               drawerLabel: ({ focused }) => (
                  <Paragraph
                     theme={focused ? 'light' : 'dark'}
                     size='large'
                     weight={focused ? '500' : undefined}
                  >
                     Favoritos
                  </Paragraph>
               ),
            }}
         />
      </Drawer.Navigator>
   );
}
