import {
   DrawerContentComponentProps,
   DrawerContentScrollView,
   DrawerItem,
   DrawerItemList,
} from '@react-navigation/drawer';
import { StyleSheet, View } from 'react-native';
import { Divider, Icon } from 'shared/components/layout';
import { Title } from 'shared/components/text';
import { Paragraph } from 'shared/components/text/paragraph';
import { theme } from 'shared/styles/theme';
const BambooCommerceImg = require('shared/assets/branding/bamboo-commerce.png');

export const CustomDrawer = (props: DrawerContentComponentProps) => {
   return (
      <DrawerContentScrollView {...props}>
         <Header />
         <DrawerItemList {...props} />
         <DrawerItem
            style={styles.logoutItem}
            label={() => (
               <Paragraph weight='400'>Acerca de Kiosko Bamboo</Paragraph>
            )}
            onPress={() => null}
         />
      </DrawerContentScrollView>
   );
};

function Header() {
   return (
      <View style={styles.headerContainer}>
         <Icon style={styles.storeImage} source={BambooCommerceImg} size={80} />
         <Title style={styles.storeName} level='h2'>
            Bodega Don Benito
         </Title>
         <Divider />
      </View>
   );
}

const styles = StyleSheet.create({
   headerContainer: {
      marginHorizontal: 10,
      marginBottom: 40,
      paddingVertical: 30,
   },
   storeImage: {
      borderRadius: 50,
      borderWidth: 2,
      borderColor: theme.colors.tertiary,
      marginBottom: 20,
   },
   storeName: {
      marginBottom: 10,
   },
   logoutItem: {
      marginTop: '75%',
   },
});
