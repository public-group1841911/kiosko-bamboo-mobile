import { NavigationProp } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { PRIVATE_SCREENS, PUBLIC_SCREENS } from '../screen-names';

/** Type checking screens */
export type PublicNavigationParamList = {
   SignInScreen: undefined;
   SignUpScreen: undefined;
   AccountRecoveryScreen: undefined;
};

export type PrivateNavigationParamList = {
   Home: undefined;
   CategoriesRoot: undefined;
   CategoriesScreen: undefined;
   OrdersScreen: undefined;
   ProductsScreen: undefined;
   FavoritesScreen: undefined;
   ShoppingCartScreen: undefined;
};

export type PublicNavigationProps = NativeStackScreenProps<
   PublicNavigationParamList,
   PUBLIC_SCREENS
>;

export type PrivateNavigationProps = NativeStackScreenProps<
   PrivateNavigationParamList,
   PRIVATE_SCREENS
>;
/** Type checking For useNavigation hook **/
export type PublicParamList = NavigationProp<PublicNavigationParamList>;
export type PrivateParamList = NavigationProp<PrivateNavigationParamList>;
