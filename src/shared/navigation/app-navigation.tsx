import { NavigationContainer } from '@react-navigation/native';
import { PrivateScreenNavigation } from './private';
import { PublicScreensNavigation } from './public';

export function AppNavigation() {
   return (
      <NavigationContainer>
         {false ? <PrivateScreenNavigation /> : <PublicScreensNavigation />}
      </NavigationContainer>
   );
}
