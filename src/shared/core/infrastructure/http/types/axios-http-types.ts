export type AxiosResp<T> = { data: T };
