export type HttpParams = { url?: string; body?: B; opts?: O };

export default abstract class HttpManager {
   abstract get<TData, B = undefined, O = undefined>(params: HttpParams): Promise<unknown>;

   abstract post<T, B = undefined, O = undefined>(url?: string, body?: B, opts?: O): Promise<T>;
}
