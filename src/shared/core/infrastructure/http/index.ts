import axios, { AxiosInstance, AxiosResponse } from 'axios';
import HttpAxiosManager, { HttpParams } from './http-manager';

export class HttpAxiosClient implements HttpAxiosManager {
   private client: AxiosInstance;
   constructor() {
      this.client = axios.create({
         method: 'post',
         baseURL: 'https://kiosko-bamboo-staging-test.onrender.com/api',
         headers: {
            'Content-type': 'application/json'
            // 'X-Access-Key': '$2b$10$9SQXMFdJqptmMuDDnnX6/.7y537SkVXjucEYeQEPBJvjotBjz44Ui'
         }
      });
      this.client.interceptors.request.use();
      this.client.interceptors.response.use();
   }

   async get<TData, B = undefined, O = undefined>({ url, body, opts }: HttpParams): Promise<AxiosResponse<TData>> {
      const resp: AxiosResponse = await this.client(url ?? '', {
         ...opts,
         data: body
      });
      return { ...resp, data: resp.data as TData };
   }

   async post<T, B = undefined, O = undefined>(url?: string, body?: B | undefined, opts?: O | undefined): Promise<T> {
      const resp: AxiosResponse = await this.client(url ?? '', {
         ...opts,
         data: body
      });
      return resp as T;
   }
}
