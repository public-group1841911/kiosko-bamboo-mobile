import { AppError } from "../errors";


type Primitives = string | number | boolean | Date;

export abstract class ValueObject<T extends Primitives> {
   readonly value: T;

   protected constructor(value: T) {
      this.value = value;
      this.ensureValueIsDefined(value);
   }

   private ensureValueIsDefined(value: T): void {
      if (value === null || value === undefined || value === '') {
         throw new AppError({ code: 'BAD_REQUEST', message: 'Value object not defined.' });
      }
   }
}
