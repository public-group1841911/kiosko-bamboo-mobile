import { randomUUID } from 'crypto';
import { type UUIDIdentifier } from '../../types/uuid';
import { ValueObject } from './value-object';

export class Identifier extends ValueObject<string> {
   constructor(value: string) {
      super(value);
      this.ensureIsValidIdentifier(value);
   }

   generate(): UUIDIdentifier {
      return randomUUID();
   }

   private ensureIsValidIdentifier(id: string): void {
      console.log(id);
   }
}
