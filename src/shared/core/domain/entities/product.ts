import { ImageSourcePropType } from "react-native";

export type Product = {
   name: string;
   img: ImageSourcePropType;
   category: { id: number; name: string; subcategory: string }[];
   net: string;
   measurement: string;
   pack: string;
   quantity: string;
   description: string
   origin: string
   brand: string;
   price: number;
   isAvailable: boolean
};