export class AppError {
   code: string;
   message: string;
   
   constructor({ code, message }: ConstructorProps) {
      this.code = code;
      this.message = message;
   }
}

type ConstructorProps = {
   code: string;
   message: string;
};
