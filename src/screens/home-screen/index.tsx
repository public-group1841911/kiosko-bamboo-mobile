import { View } from 'react-native';
import { TextField } from 'shared/components/inputs';
import { Box, Layout } from 'shared/components/layout';
import { theme } from 'shared/styles/theme';
import { AdvertisementSlider } from './components/advertisement-slider';
import { CategoriesSlider } from './components/categories-slider';
import { PopularSlider } from './components/popular-slider';
import { SpecialsList } from './components/specials-list';
import { ScrollView } from 'react-native-gesture-handler';
import { metrics } from 'shared/styles/metrics';
const SearchIco = require('shared/assets/forms/search-inactive.png');
const Offers1Img = require('shared/assets/mocks/offer-1.png');
const Offers2Img = require('shared/assets/mocks/offer-2.png');
const Offers3Img = require('shared/assets/mocks/offer-3.png');
const CokeDietPackImg = require('shared/assets/mocks/coke-diet-pack.png');
const OreoPackImg = require('shared/assets/mocks/oreo-pack.png');
const PirulinPackImg = require('shared/assets/mocks/pirulin-pack.png');
const SoyPackImg = require('shared/assets/mocks/soy-sauce-pack.png');
const PeppersImg = require('shared/assets/mocks/peppers.png');
const VeggiesImg = require('shared/assets/mocks/veggies.png');
const FruitsImg = require('shared/assets/mocks/fruits.png');
const BeansImg = require('shared/assets/mocks/beans.png');
const advertisementImages = [Offers1Img, Offers2Img, Offers3Img];

const mostPopular = [
   {
      name: 'Cocacola Diet 6 Pack',
      img: CokeDietPackImg,
      size: 29,
      price: 0.99,
   },
   {
      name: 'Oreo tubular 30 unidades ',
      img: OreoPackImg,
      size: 26,
      price: 0.99,
   },
   { name: 'Pirulin de coco', img: PirulinPackImg, size: 22, price: 0.99 },
   { name: 'Salsa de soya Pekin 150ml', img: SoyPackImg, size: 28, price: 0.99 },
];

const specials = [
   {
      name: 'Cocacola Diet 6 Pack',
      img: VeggiesImg,
      size: 160,
      color: '#ffc05c',
   },
   {
      name: 'Oreo tubular 30 unidades ',
      img: PeppersImg,
      size: 140,
      color: '#009e9a',
   },
   { name: 'Pirulin de coco', img: BeansImg, size: 200, color: '#8f5a8b' },
   {
      name: 'Salsa de soya Pekin 150ml',
      img: FruitsImg,
      size: 170,
      color: '#4da159',
   },
];

export const HomeScreen = () => {
   return (
      <Layout paddingTop={metrics.headerHeight}>
         <View
            style={{
               paddingHorizontal: 15,
               backgroundColor: theme.colors.primary,
            }}
         >
            <TextField
               placeholder='Buscar'
               name=''
               value=''
               theme='light'
               ico={SearchIco}
               sizeIco={22}
               invertIco
            />
         </View>
         <ScrollView
            style={{ flex: 1 }}
            bounces={false}
            decelerationRate='fast'
            showsVerticalScrollIndicator={false}
         >
            <AdvertisementSlider images={advertisementImages} />
            <Box flex={1} paddingLeft={10}>
               <CategoriesSlider />
               <PopularSlider products={mostPopular} />
               <SpecialsList specials={specials} />
            </Box>
         </ScrollView>
      </Layout>
   );
};
