import { FC } from 'react';
import { ImageSourcePropType, ScrollView, View } from 'react-native';
import { Title } from 'shared/components/text';
import { SpecialsCard } from './special-card';

type SpecialsListProps = {
   specials: {
      name: string;
      img: ImageSourcePropType;
      size: number;
      color: string
   }[];
};

export const SpecialsList: FC<SpecialsListProps> = ({ specials }) => {
   return (
      <View style={{ flex: 1 }}>
         <Title level='h2' theme='light'>
            Especiales para ti
         </Title>

         <ScrollView style={{  marginRight: 10 }}>
            {specials.map(special => (
               <SpecialsCard key={special.name} special={special} />
            ))}
         </ScrollView>
      </View>
   );
};
