import { FC } from 'react';
import {
   Dimensions,
   Image,
   ImageSourcePropType,
   StyleSheet,
   Text,
   TouchableOpacity,
   View,
} from 'react-native';
import { theme } from 'shared/styles/theme';
const SCREEN_WIDTH = Dimensions.get('window').width;
type SpecialCardProps = {
   special: {
      name: string;
      img: ImageSourcePropType;
      size: number;
      color: string;
   };
};
export const SpecialsCard: FC<SpecialCardProps> = ({ special }) => {
   return (
      <TouchableOpacity
         style={{ ...styles.card, backgroundColor: special.color }}
         activeOpacity={1}
      >
         <View style={styles.label}>
            <Text style={styles.labelText}>Especiales</Text>
         </View>
         <Image
            style={{
               ...styles.image,
               height: special.size,
               width: special.size,
            }}
            source={special.img}
         />
      </TouchableOpacity>
   );
};

const styles = StyleSheet.create({
   card: {
      height: 170,
      width: SCREEN_WIDTH,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 5,
      paddingHorizontal: 10,
      borderRadius: 8,
      marginBottom: 10,
      backgroundColor: theme.colors.background,
      overflow: 'hidden',

      //...theme.boxShadow,
   },
   image: {
      resizeMode: 'contain',
   },
   label: {
      top: 15,
      left: -40,
      transform: [{ rotate: '-45deg' }],
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      height: 40,
      width: 150,
      backgroundColor: '#ff3434',
   },
   labelText: {
      fontSize: 16,
      fontWeight: '600',
   },
});
