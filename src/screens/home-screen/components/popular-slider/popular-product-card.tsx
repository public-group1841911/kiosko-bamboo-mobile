import { FC, useState } from 'react';
import {
   Image,
   ImageSourcePropType,
   StyleSheet,
   Text,
   TouchableOpacity,
   View,
} from 'react-native';
import { Title } from 'shared/components/text';
import { theme } from 'shared/styles/theme';
const FavoriteOutlineIco = require('shared/assets/product/favorite-outline-ico.png');
const FavoriteFillIco = require('shared/assets/product/favorite-fill-ico.png');

type PopularProductCardProps = {
   product: {
      name: string;
      img: ImageSourcePropType;
      size: number;
      price: number;
      quantity?: number;
   };
};
export const PopularProductCard: FC<PopularProductCardProps> = ({ product }) => {
   const [isFavorite, setIsFavorite] = useState(false);

   const setFavorite = () => setIsFavorite(!isFavorite);

   return (
      <TouchableOpacity style={styles.card} activeOpacity={0.6}>
         <TouchableOpacity
            activeOpacity={0.6}
            style={styles.favorite}
            onPress={setFavorite}
         >
            <Image
               source={isFavorite ? FavoriteFillIco : FavoriteOutlineIco}
               style={styles.favoriteIco}
            />
         </TouchableOpacity>
         <Image
            style={{ height: 80, width: 150, resizeMode: 'contain' }}
            source={product.img}
         />
         <View style={styles.content}>
            <Title level='h3' theme='light'>
               {product.name}
            </Title>
            <Text style={styles.price}>${product.price}</Text>
         </View>
      </TouchableOpacity>
   );
};

const styles = StyleSheet.create({
   card: {
      height: 170,
      width: 180,
      marginRight: 15,
      paddingTop: 5,
      paddingHorizontal: 10,
      borderRadius: 8,
      marginVertical: 10,
      backgroundColor: theme.colors.background,
      ...theme.boxShadow,
   },
   favorite: {
      zIndex: 1,
      height: 45,
      width: 50,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      right: 0,
      borderTopRightRadius: 8,
   },
   favoriteIco: {
      height: 22,
      width: 22,
   },
   content: {
      flex: 1,
      justifyContent: 'space-between',
      paddingBottom: 5,
   },
   price: {
      color: theme.colors.secondary,
      fontSize: 16,
   },
});
