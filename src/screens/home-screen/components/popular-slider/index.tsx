import { FC } from 'react';
import { ImageSourcePropType, ScrollView, View } from 'react-native';
import { Title } from 'shared/components/text';
import { PopularProductCard } from './popular-product-card';

type PopularSliderProps = {
   products: {
      name: string;
      img: ImageSourcePropType;
      size: number;
      price: number;
      quantity?: number;
   }[];
};

export const PopularSlider: FC<PopularSliderProps> = ({ products }) => {
   return (
      <View style={{ flex: 1, marginBottom: 20 }}>
         <Title level='h2' theme='light'>
            Más populares
         </Title>

         <ScrollView
            style={{ flex: 1 }}
            horizontal
            showsHorizontalScrollIndicator={false}
         >
            {products.map(product => (
               <PopularProductCard key={product.name} product={product} />
            ))}
         </ScrollView>
      </View>
   );
};
