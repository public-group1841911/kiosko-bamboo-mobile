import { FC } from 'react';
import {
   ImageSourcePropType,
   StyleSheet,
   Text,
   TouchableOpacity,
   View,
} from 'react-native';
import { Icon } from 'shared/components/layout';
import { theme } from 'shared/styles/theme';

type CategoryButtonProps = {
   category: {
      name: string;
      img: ImageSourcePropType;
      size: number;
   };
};

export const CategoryButton: FC<CategoryButtonProps> = ({ category }) => {
   return (
      <View key={category.name} style={styles.buttonContainer}>
         <TouchableOpacity style={styles.button} activeOpacity={0.6}>
            <Icon source={category.img} size={category.size} />
         </TouchableOpacity>
         <Text style={styles.name}>{category.name}</Text>
      </View>
   );
};

const styles = StyleSheet.create({
   buttonContainer: {
      alignItems: 'center',
      gap: 5,
   },
   button: {
      alignItems: 'center',
      justifyContent: 'center',
      height: 50,
      width: 50,
      marginHorizontal: 14,
      borderRadius: 25,
      backgroundColor: theme.colors.background,
      ...theme.boxShadow,
   },
   name: {
      color: theme.colors.secondary,
   },
});
