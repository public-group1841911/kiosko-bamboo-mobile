import { ScrollView, StyleSheet, View } from 'react-native';
import { CategoryButton } from './category-button';
const BreadIco = require('shared/assets/categories/ico/bread-ico.png');
const FruitIco = require('shared/assets/categories/ico/fruit-ico.png');
const CheeseIco = require('shared/assets/categories/ico/cheese-ico.png');
const DrinkIco = require('shared/assets/categories/ico/drink-ico.png');
const CandyIco = require('shared/assets/categories/ico/candy-ico.png');
const SaltIco = require('shared/assets/categories/ico/salt-ico.png');
const PlusIco = require('shared/assets/categories/ico/plus-ico.png');
const categories = [
   { name: 'Bebidas', img: DrinkIco, size: 29 },
   { name: 'Confite', img: CandyIco, size: 26 },
   { name: 'Despensa', img: SaltIco, size: 22 },
   { name: 'Frutas', img: FruitIco, size: 28 },
   { name: 'Lácteos', img: CheeseIco, size: 23 },
   { name: 'Panadería', img: BreadIco, size: 28 },
   { name: 'Más', img: PlusIco, size: 20 },
];

export const CategoriesSlider = () => {
   return (
      <View style={styles.container}>
         <ScrollView showsHorizontalScrollIndicator={false} horizontal>
            {categories.map(category => (
               <CategoryButton key={category.name} category={category} />
            ))}
         </ScrollView>
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      paddingVertical: 15,
   },
});
