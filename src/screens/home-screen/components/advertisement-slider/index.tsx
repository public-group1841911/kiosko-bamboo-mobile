import { FC, useRef, useState } from 'react';
import {
   Image,
   ImageSourcePropType,
   Pressable,
   ScrollView,
   Text,
   View,
   useWindowDimensions,
} from 'react-native';
import { theme } from 'shared/styles/theme';
import { styles } from './slider-styles';

type AdvertisementSliderProps = {
   images: ImageSourcePropType[];
};

export const AdvertisementSlider: FC<AdvertisementSliderProps> = ({
   images,
}) => {
   const { width: SCREEN_WIDTH } = useWindowDimensions();
   const [slideImg, setSlideImg] = useState(0);

   const slideRef = useRef<ScrollView>(null);

   const slideTo = (index: number) => {
      if (slideRef.current) {
         slideRef.current.scrollTo({ x: Number(SCREEN_WIDTH) * index });
      }
   };

   return (
      <View
         style={{
            backgroundColor: theme.colors.primary,
         }}
      >
         <ScrollView
            ref={slideRef}
            horizontal
            decelerationRate='fast'
            onScroll={({ nativeEvent }) => {
               const offset = nativeEvent.contentOffset.x / SCREEN_WIDTH;
               setSlideImg(Math.round(offset));
            }}
            showsHorizontalScrollIndicator={false}
            snapToInterval={SCREEN_WIDTH}
         >
            {images.map((image, index) => (
               <Image key={index} style={styles.image} source={image} />
            ))}
         </ScrollView>
         <View style={styles.controls}>
            {images.map((_, index) => (
               <Pressable key={index} onPress={() => slideTo(index)}>
                  <Text
                     style={{
                        paddingHorizontal: 4,
                        fontSize: slideImg === index ? 34 : 18,
                     }}
                  >
                     {slideImg === index ? '∘' : '●'}
                  </Text>
               </Pressable>
            ))}
         </View>
      </View>
   );
};
