import { Dimensions, StyleSheet } from 'react-native';
import { theme } from 'shared/styles/theme';
const SCREEN_WIDTH = Dimensions.get('window').width;
export const styles = StyleSheet.create({
   image: {
      height: 150,
      width: SCREEN_WIDTH,
      resizeMode: 'contain',
   },
   controls: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.colors.primary,
   },
});
