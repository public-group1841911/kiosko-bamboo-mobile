import { FC } from 'react';
import { Text, View } from 'react-native';
import { Title } from 'shared/components/text';
import { Paragraph } from 'shared/components/text/paragraph';
import { styles } from './product-card-styles';

type CardDescriptionProps = {
   name: string;
   net: string;
   measurement: string;
   pack: string;
   brand: string;
   quantity: string;
};
const Description: FC<CardDescriptionProps> = ({
   name,
   net,
   measurement,
   pack,
   quantity,
   brand,
}) => {
   return (
      <View style={styles.content}>
         <Title style={styles.title} level='h2'>
            {name} - {net + measurement}
         </Title>
         <Text style={styles.pack}>
            {pack} - {quantity} unidades
         </Text>
         <Paragraph style={styles.factory} size='small'>
            De: {brand}
         </Paragraph>
      </View>
   );
};

export default Description;
