import { StyleSheet } from 'react-native';
import { theme } from 'shared/styles/theme';

export const styles = StyleSheet.create({
   unavailable: {
      opacity: 0.3,
   },
   unavailableLabel: {
      paddingVertical: 4,
      paddingHorizontal: 10,
      zIndex: 1,
      position: 'absolute',
      color: '#fff',
      backgroundColor: '#9e3e3e',
      borderTopLeftRadius: 20,
      borderBottomRightRadius: 20,
   },
   container: {
      marginTop: 10,
      marginHorizontal: 10,
      marginBottom: 20,
      backgroundColor: '#dfe0dd',
      borderBottomRightRadius: 20,
      borderTopLeftRadius: 20,
   },
   card: {
      paddingTop: 5,
      backgroundColor: '#dfe0dd',
      borderBottomRightRadius: 20,
      borderTopLeftRadius: 20,
      overflow: 'hidden',
      ...theme.boxShadow,
   },
   favoriteButton: {
      zIndex: 1,
      padding: 8,
      position: 'absolute',
      right: 0,
   },
   image: {
      alignSelf: 'center',
      height: 100,
      resizeMode: 'contain',
   },
   content: {
      paddingHorizontal: 10,
   },
   title: {
      zIndex: 1,
      color: theme.colors.secondary,
   },
   pack: {
      zIndex: 1,
      color: theme.colors.secondary,
   },
   factory: {
      zIndex: 1,
      color: theme.colors.onTertiary,
   },
   price: {
      color: theme.colors.onTertiary,
   },
   addButton: {
      position: 'absolute',
      paddingVertical: 3,
      paddingHorizontal: 35,
      backgroundColor: theme.colors.primary,
      borderBottomRightRadius: 20,
      borderTopLeftRadius: 20,
      right: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
   },
   addIco: {
      fontSize: 18,
      verticalAlign: 'middle',
   },
});
