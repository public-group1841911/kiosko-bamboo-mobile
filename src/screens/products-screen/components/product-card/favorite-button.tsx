import { Dispatch, FC, SetStateAction } from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'shared/components/layout';
import { styles } from './product-card-styles';

const FavoriteOutlineIco = require('shared/assets/product/favorite-outline-ico.png');
const FavoriteFillIco = require('shared/assets/product/favorite-fill-ico.png');
type FavoriteButtonProps = {
   isAvailable: boolean;
   isFavorite: boolean;
   setFavorite: Dispatch<SetStateAction<boolean>>;
};

export const FavoriteButton: FC<FavoriteButtonProps> = ({
   isAvailable,
   isFavorite,
   setFavorite,
}) => {
   return (
      <TouchableOpacity
         disabled={!isAvailable}
         style={styles.favoriteButton}
         onPress={() => setFavorite(!isFavorite)}
      >
         <Icon source={isFavorite ? FavoriteFillIco : FavoriteOutlineIco} />
      </TouchableOpacity>
   );
};
