import { FC } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Paragraph } from 'shared/components/text/paragraph';
import { styles } from './product-card-styles';

type PriceProps = {
   price: number;
   isAvailable: boolean;
};
export const Price: FC<PriceProps> = ({ price, isAvailable }) => {
   return (
      <>
         <Paragraph style={styles.price} size='large' weight='500'>
            ${price}
         </Paragraph>
         <TouchableOpacity style={styles.addButton} disabled={isAvailable}>
            <Text style={styles.addIco}>+</Text>
         </TouchableOpacity>
      </>
   );
};
