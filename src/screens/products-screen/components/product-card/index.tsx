import { FC, useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Product } from 'shared/core/domain/entities/product';
import Description from './description';
import { FavoriteButton } from './favorite-button';
import { Price } from './price';
import { styles } from './product-card-styles';

type ProductCardProps = {
   product: Product & { isFavorite: boolean };
   onPress(product: Product & { isFavorite: boolean }): void;
};

export const ProductCard: FC<ProductCardProps> = ({ product, onPress }) => {
   const [isFavorite, setFavorite] = useState(product.isFavorite);
   return (
      <View style={styles.container}>
         {!product.isAvailable && (
            <Text style={styles.unavailableLabel}>No disponible</Text>
         )}
         <TouchableOpacity
            style={[
               { ...styles.card },
               !product.isAvailable && styles.unavailable,
            ]}
            activeOpacity={0.6}
            onPress={() => onPress({ ...product, isFavorite })}
         >
            <FavoriteButton
               isAvailable={product.isAvailable}
               isFavorite={isFavorite}
               setFavorite={setFavorite}
            />

            <Image style={{ ...styles.image }} source={product.img} />
            <Description
               name={product.name}
               brand={product.brand}
               net={product.net}
               pack={product.pack}
               quantity={product.quantity}
               measurement={product.measurement}
            />
            <Price price={product.price} isAvailable={product.isAvailable} />
         </TouchableOpacity>
      </View>
   );
};
