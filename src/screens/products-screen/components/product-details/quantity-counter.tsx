import { FC } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { styles } from './product-details-styles';

type QuantityCounterProps = {
   isAvailable: boolean;
   price: number;
   quantity: number;
   setProductQuantity(value: boolean): void;
};

export const QuantityCounter: FC<QuantityCounterProps> = ({
   isAvailable,
   price,
   quantity,
   setProductQuantity,
}) => {
   return (
      <View style={styles.quantityContainer}>
         <View
            style={[
               styles.quantityControllers,
               (!isAvailable || quantity === 0) && styles.disabled,
            ]}
         >
            <TouchableOpacity
               disabled={!isAvailable || quantity === 0}
               style={styles.quantityButton}
               onPress={() => setProductQuantity(false)}
            >
               <Text style={styles.quantitySymbol}>-</Text>
            </TouchableOpacity>
            <View style={styles.quantity}>
               <Text>{quantity}</Text>
            </View>
            <TouchableOpacity
               disabled={!isAvailable || quantity === 0}
               style={styles.quantityButton}
               onPress={() => setProductQuantity(true)}
            >
               <Text style={styles.quantitySymbol}>+</Text>
            </TouchableOpacity>
         </View>
         <Text style={[styles.price, !isAvailable && styles.priceDashed]}>
            ${quantity > 1 ? price * quantity : price}
         </Text>
      </View>
   );
};
