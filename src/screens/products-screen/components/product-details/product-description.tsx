import { FC } from 'react';
import { Text } from 'react-native';
import { Title } from 'shared/components/text';
import { Paragraph } from 'shared/components/text/paragraph';
import { styles } from './product-details-styles';

type ProductDescriptionProps = {
   name: string;
   net: string;
   measurement: string;
   description: string;
   pack: string;
   quantity: string;
   brand: string;
   origin: string;
};

export const ProductDescription: FC<ProductDescriptionProps> = ({
   name,
   net,
   measurement,
   description,
   pack,
   quantity,
   brand,
   origin,
}) => {
   return (
      <>
         <Title style={styles.productName}>
            {name + ' '}
            {net + measurement}
         </Title>
         <Paragraph style={styles.description} size='normal'>
            {description}
         </Paragraph>
         <Text style={styles.detailText}>
            <Paragraph style={styles.detailName} weight='400'>
               {pack}:
            </Paragraph>{' '}
            {quantity} unidades.
         </Text>
         <Text style={styles.detailText}>
            <Paragraph style={styles.detailName} weight='400'>
               Neto:
            </Paragraph>{' '}
            {net + measurement} por unidad.
         </Text>
         <Text style={styles.detailText}>
            <Paragraph style={styles.detailName} weight='400'>
               Fabricado por:
            </Paragraph>{' '}
            {brand}.
         </Text>
         <Text style={styles.detailText}>
            <Paragraph style={styles.detailName} weight='400'>
               Producto:
            </Paragraph>{' '}
            {origin}.
         </Text>
      </>
   );
};
