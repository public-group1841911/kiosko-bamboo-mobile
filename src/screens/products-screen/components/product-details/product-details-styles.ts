import { StyleSheet } from 'react-native';
import { theme } from 'shared/styles/theme';

export const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: theme.colors.surface,
      alignItems: 'center',
      justifyContent: 'flex-end',
   },
   favoriteButton: {
      zIndex: 1,
      padding: 8,
      position: 'absolute',
      top: 0,
      right: 0,
   },
   image: {
      marginTop: 50,
      zIndex: -1,
      flex: 1,
      height: 250,
      resizeMode: 'contain',
   },
   content: {
      paddingTop: 5,
      flex: 1.3,
      width: '100%',
      marginTop: 40,
      paddingHorizontal: 20,
      backgroundColor: theme.colors.primary,
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
   },
   /** Product Description component**/
   productName: {
      flex: 1.2,
      alignSelf: 'center',
   },
   description: {
      flex: 1,
      color: theme.colors.surface,
   },

   detailName: {
      flex: 1,
      fontSize: 18,
      fontWeight: '400',
   },
   detailText: {
      flex: 1,
      fontSize: 16,
   },
   /** Quantity counter component**/
   quantityContainer: {
      paddingVertical: 20,
      flex: 1.4,
      flexDirection: 'row',
      justifyContent: 'space-between',
   },
   quantityControllers: {
      flexDirection: 'row',
   },
   quantity: {
      height: 40,
      width: 40,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.colors.secondary,
      borderRadius: 10,
   },
   quantityButton: {
      height: 40,
      width: 38,
      alignItems: 'center',
      justifyContent: 'center',
   },
   quantitySymbol: {
      fontSize: 25,
      color: theme.colors.tertiary,
   },
   price: {
      fontWeight: '500',
      fontSize: 26,
      color: theme.colors.surface,
   },
   unavailable: {
      textAlign: 'center',
      marginBottom: 10,
      paddingVertical: 10,
      paddingHorizontal: 10,
      color: theme.colors.surface,
      backgroundColor: theme.colors.error,
      borderTopLeftRadius: 20,
      borderBottomRightRadius: 20,
   },
   disabled: {
      opacity: 0.4,
   },
   priceDashed: {
      color: theme.colors.error,
      textDecorationLine: 'line-through',
   },
});
