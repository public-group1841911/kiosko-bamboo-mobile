import { Dispatch, FC, SetStateAction, useState } from 'react';
import { Image, Modal, TouchableOpacity, View } from 'react-native';
import { BackButton, LeafButton } from 'shared/components/buttons';
import { Icon } from 'shared/components/layout';
import { Paragraph } from 'shared/components/text/paragraph';
import { Product } from 'shared/core/domain/entities/product';
import { ProductDescription } from './product-description';
import { styles } from './product-details-styles';
import { QuantityCounter } from './quantity-counter';
const FavoriteOutlineIco = require('shared/assets/product/favorite-outline-ico.png');
const FavoriteFillIco = require('shared/assets/product/favorite-fill-ico.png');
type ProductDetailBottomSheetProps = {
   product: Product & { isFavorite: boolean };
   isVisible: boolean;
   setIsFavorite: Dispatch<SetStateAction<boolean>>;
   setVisibility: Dispatch<SetStateAction<boolean>>;
};

export const ProductDetails: FC<ProductDetailBottomSheetProps> = ({
   product,
   isVisible,
   setVisibility,
   setIsFavorite,
}) => {
   const [quantity, setQuantity] = useState(0);

   const setProductQuantity = (isAdd: boolean) => {
      if (quantity === 0 && !isAdd) return;
      setQuantity(currentQty => (isAdd ? currentQty + 1 : currentQty - 1));
   };

   return (
      <Modal visible={isVisible} animationType='slide'>
         <View style={styles.container}>
            <BackButton goBack={() => setVisibility(!isVisible)} />
            <TouchableOpacity
               style={styles.favoriteButton}
               onPress={() => setIsFavorite(!product.isFavorite)}
            >
               <Icon
                  source={
                     product.isFavorite ? FavoriteFillIco : FavoriteOutlineIco
                  }
                  size={30}
               />
            </TouchableOpacity>
            <Image style={styles.image} source={product.img} />
            <View style={styles.content}>
               <ProductDescription
                  name={product.name}
                  description={product.description}
                  measurement={product.measurement}
                  brand={product.brand}
                  net={product.net}
                  origin={product.origin}
                  pack={product.pack}
                  quantity={product.quantity}
               />
               <QuantityCounter
                  price={product.price}
                  quantity={quantity}
                  isAvailable={product.isAvailable}
                  setProductQuantity={setProductQuantity}
               />
               <View>
                  {product.isAvailable ? (
                     <LeafButton
                        text={
                           quantity === 0
                              ? 'Añadir al carrito'
                              : 'Quitar del carrito'
                        }
                        theme={quantity === 0 ? 'light' : 'error'}
                        onPress={() => {
                           if (quantity !== 0) {
                              setQuantity(0);
                              return;
                           }
                           setProductQuantity(true);
                        }}
                     />
                  ) : (
                     <Paragraph style={styles.unavailable} weight='400'>
                        No disponible
                     </Paragraph>
                  )}
               </View>
            </View>
         </View>
      </Modal>
   );
};
