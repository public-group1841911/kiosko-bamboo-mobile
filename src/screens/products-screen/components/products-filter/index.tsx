import { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'shared/components/layout';
import { Paragraph } from 'shared/components/text/paragraph';
import { theme } from 'shared/styles/theme';
const FilterIco = require('shared/assets/product/product-filter-ico.png');
const CloseIco = require('shared/assets/navigation/close-filter-ico.png');

type ProductFilterProps = {
   criteria: string;
   onPress(): void;
   onClearFilter(): void;
};

export const ProductsFilter: FC<ProductFilterProps> = ({
   criteria,
   onPress,
   onClearFilter,
}) => {
   return (
      <View style={styles.container}>
         {criteria ? (
            <TouchableOpacity style={styles.criteria} onPress={onClearFilter}>
               <Paragraph theme='light' weight='400'>{criteria} </Paragraph>
               <Icon source={CloseIco} size={18} />
            </TouchableOpacity>
         ) : (
            <Paragraph theme='light'>Todos</Paragraph>
         )}
         <TouchableOpacity style={{ padding: 10 }} onPress={onPress}>
            <Icon source={FilterIco} />
         </TouchableOpacity>
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      paddingHorizontal: 10,
   },
   criteria: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 15,
      borderWidth: 1,
      borderColor: theme.colors.secondary,
      color: theme.colors.onTertiary,
      borderBottomRightRadius: 20,
      borderTopLeftRadius: 20,
   },
});
