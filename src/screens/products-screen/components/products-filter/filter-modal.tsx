import { FC } from 'react';
import { Modal, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Icon } from 'shared/components/layout';

import { Title } from 'shared/components/text';
import { Paragraph } from 'shared/components/text/paragraph';
import { theme } from 'shared/styles/theme';
const FilterIco = require('shared/assets/product/product-filter-ico-modal.png');
const CloseIco = require('shared/assets/navigation/close-ico.png');

type FilterModalProps = {
   title: string;
   subcategories: { title: string }[];
   isModalVisible: boolean;
   onClose(): void;
   onSelect(criteria: string): void;
};

export const FilterModal: FC<FilterModalProps> = ({
   title,
   isModalVisible,
   subcategories,
   onClose,
   onSelect,
}) => {
   return (
      <Modal visible={isModalVisible} transparent animationType='fade'>
         <View style={styles.backdrop}>
            <View style={styles.modal}>
               <TouchableOpacity style={styles.closeButton} onPress={onClose}>
                  <Icon source={CloseIco} size={22} />
               </TouchableOpacity>
               <View style={styles.header}>
                  <Title style={styles.headerTitle} level='h2'>
                     Filtrar {title.toLowerCase()} por:
                  </Title>
                  <Icon source={FilterIco} size={22} />
               </View>
               <View style={styles.content}>
                  {subcategories.map(({ title }) => (
                     <TouchableOpacity
                        key={title}
                        style={styles.subcategory}
                        onPress={() => {
                           onSelect(title);
                           onClose();
                        }}
                     >
                        <Paragraph style={styles.subcategoryText} weight='500'>
                           {title}
                        </Paragraph>
                     </TouchableOpacity>
                  ))}
               </View>
            </View>
         </View>
      </Modal>
   );
};

const styles = StyleSheet.create({
   backdrop: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: theme.colors.backdrop,
   },
   modal: {
      alignItems: 'center',
      paddingVertical: 20,
      paddingHorizontal: 20,
      marginHorizontal: 20,
      borderRadius: 10,
      backgroundColor: theme.colors.surface,
      ...theme.boxShadow,
   },
   closeButton: {
      padding: 10,
      alignSelf: 'flex-end',
      top: -15,
      right: -15,
   },
   header: { alignItems: 'center', gap: 5 },
   headerTitle: {
      color: theme.colors.onTertiary,
   },
   content: {
      width: '100%',
      justifyContent: 'center',
      flexDirection: 'row',
      flexWrap: 'wrap',
      gap: 20,
      marginTop: 20,
   },
   subcategory: {
      paddingVertical: 5,
      paddingHorizontal: 10,
      borderRadius: 25,
      backgroundColor: theme.colors.tertiary,
      ...theme.boxShadow,
   },
   subcategoryText: { color: theme.colors.onTertiary },
});
