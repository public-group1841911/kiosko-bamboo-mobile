import { Product } from "shared/core/domain/entities/product";

const PepsiImg = require('./img/pepsi.png');
const CokeImg = require('./img/coke.png');
const GoldenImg = require('./img/golden.png');
const FruitPunchImg = require('./img/fruit-punch.png');
const GuavaImg = require('./img/guava.png');
const MonsterImg = require('./img/monster.png');
const RedBullImg = require('./img/red-bull.png');

export const products: Product[] = [
   {
      name: 'Pepsi',
      img: PepsiImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Gaseosas' }],
      net: '2',
      description: 'Gaseosa sabor a cola negra.',
      measurement: 'Lts',
      pack: 'Empaque',
      quantity: '6',
      brand: 'Pepsicola',
      origin: 'Nacional',
      isAvailable: false,
      price: 4.99,
   },
   {
      name: 'Coca cola',
      img: CokeImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Gaseosas' }],
      net: '2',
      description: 'Refresco de cola negra con gas.',
      measurement: 'Lts',
      pack: 'Empaque',
      quantity: '6',
      brand: 'Pepsicola',
      origin: 'Nacional',
      isAvailable: true,
      price: 4.99,
   },
   {
      name: 'Red bull',
      img: RedBullImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Energéticas' }],
      net: '250',
      description:
         'Bebida energizante ideal para atletas o agotamiento físico.',
      measurement: 'ml',
      pack: 'Empaque',
      quantity: '6',
      brand: 'Red bull',
      origin: 'Importado',
      isAvailable: true,
      price: 9.69,
   },
   {
      name: 'Jugo de guayaba',
      img: GuavaImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Jugos' }],
      net: '1.9',
      description: 'Concentrado de jugo de guayaba industrial.',
      measurement: 'Lts',
      pack: 'Empaque',
      quantity: '8',
      brand: 'Los Andes',
      origin: 'Nacional',
      isAvailable: true,
      price: 6.99,
   },
   {
      name: 'Jugo Fruit Punch',
      img: FruitPunchImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Jugos' }],
      net: '1.9',
      description: 'Jugo con sabor a frutal tropicales.',
      measurement: 'Lts',
      pack: 'Empaque',
      quantity: '8',
      brand: 'Los Andes',
      origin: 'Nacional',
      isAvailable: true,
      price: 7.89,
   },
   {
      name: 'Monster',
      img: MonsterImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Energéticas' }],
      net: '250',
      description:
         'Bebida energizante ideal para atletas o agotamiento físico.',
      measurement: 'ml',
      pack: 'Empaque',
      quantity: '12',
      brand: 'Monster',
      origin: 'Importado',
      isAvailable: true,
      price: 10.69,
   },
   {
      name: 'Manzanita Golden',
      img: GoldenImg,
      category: [{ id: 1, name: 'Bebidas', subcategory: 'Gaseosas' }],
      net: '1.5',
      description: 'Gaseosa sabor a manzana.',
      measurement: 'Lts',
      pack: 'Empaque',
      quantity: '8',
      brand: 'Pepsicola',
      origin: 'Nacional',
      isAvailable: true,
      price: 4.79,
   },
];
