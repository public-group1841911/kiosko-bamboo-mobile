import { MasonryFlashList } from '@shopify/flash-list';
import { useState } from 'react';
import { View } from 'react-native';
import { TextField } from 'shared/components/inputs';
import { Layout } from 'shared/components/layout';
import { Product } from 'shared/core/domain/entities/product';
import { metrics } from 'shared/styles/metrics';
import { theme } from 'shared/styles/theme';
import { ProductCard } from './components/product-card';
import { ProductDetails } from './components/product-details';
import { ProductsFilter } from './components/products-filter';
import { FilterModal } from './components/products-filter/filter-modal';
import { products as productsMocks } from './mockData';
const SearchIco = require('shared/assets/forms/search-inactive.png');

export const ProductsScreen = () => {
   const [criteria, setCriteria] = useState<string>('');
   const [isModalVisible, setIsModalVisible] = useState(false);
   const [isDetailVisible, setIsDetailVisible] = useState(false);
   const [isFavorite, setIsFavorite] = useState(false);
   const showFilterModal = () => setIsModalVisible(!isModalVisible);

   const showDetails = (product: Product & { isFavorite: boolean }) => {
      setProduct({ ...product, isFavorite: product.isFavorite });
      setIsDetailVisible(true);
   };
   const [products, setProducts] = useState(productsMocks);
   const [product, setProduct] = useState<Product & { isFavorite: boolean }>({
      ...({} as Product),
      isFavorite,
   });
   const [filteredProducts, setFilteredProducts] = useState(productsMocks);
   const setFilter = (criteria: string = '') => {
      setCriteria(criteria);
      setFilteredProducts(
         products.filter(product =>
            product.category[0].subcategory
               .toLowerCase()
               .includes(criteria.toLowerCase())
         )
      );
   };

   return (
      <Layout paddingTop={metrics.headerHeight}>
         <View
            style={{
               backgroundColor: theme.colors.primary,
               marginBottom: 15,
               paddingHorizontal: 10,
            }}
         >
            <TextField
               placeholder='Buscar'
               name=''
               value=''
               theme='light'
               ico={SearchIco}
               sizeIco={22}
               invertIco
            />
         </View>
         <FilterModal
            title={productsMocks[0].category[0].name}
            isModalVisible={isModalVisible}
            subcategories={[
               { title: 'Gaseosas' },
               { title: 'Energéticas' },
               { title: 'Jugos' },
            ]}
            onClose={showFilterModal}
            onSelect={setFilter}
         />
         <ProductsFilter
            criteria={criteria}
            onPress={showFilterModal}
            onClearFilter={setFilter}
         />
         <MasonryFlashList
            data={filteredProducts}
            numColumns={2}
            keyExtractor={item => item.name}
            estimatedItemSize={230}
            renderItem={({ item: product }) => (
               <ProductCard
                  product={{ ...product, isFavorite }}
                  onPress={showDetails}
               />
            )}
         />
         
         <ProductDetails
            product={product}
            isVisible={isDetailVisible}
            setVisibility={setIsDetailVisible}
            setIsFavorite={setIsFavorite}
         />
      </Layout>
   );
};
