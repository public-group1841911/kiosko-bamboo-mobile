import { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { LeafButton } from 'shared/components/buttons';
import { GridView, Layout } from 'shared/components/layout';
import { Title } from 'shared/components/text';
import { metrics } from 'shared/styles/metrics';
import { theme } from 'shared/styles/theme';
import { products } from '../products-screen/mockData';
import ProductCard from './components/product-card';

export const ShoppingCartScreen = () => {
   const [quantity, setQuantity] = useState(0);

   const setProductQuantity = (isAdd: boolean) => {
      if (quantity === 0 && !isAdd) return;
      setQuantity(currentQty => (isAdd ? currentQty + 1 : currentQty - 1));
   };
   return (
      <Layout paddingTop={metrics.headerHeight} paddingHorizontal={10}>
         <Title
            level='h2'
            theme='light'
            style={{ paddingVertical: 10, color: theme.colors.secondary }}
         >
            Carrito de compra
         </Title>
         <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
            <GridView
               col={1}
               rowGap={10}
               data={products}
               renderItem={product => <ProductCard product={product} />}
            />
         </ScrollView>
         <Title
            theme='light'
            style={{ paddingVertical: 10, color: theme.colors.secondary }}
         >
            Total: $44,99
         </Title>
         <LeafButton text='Pagar' theme='primary' onPress={() => {}} />
      </Layout>
   );
};

const styles = StyleSheet.create({});
