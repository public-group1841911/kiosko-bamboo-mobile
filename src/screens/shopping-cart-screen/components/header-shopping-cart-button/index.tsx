import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native";
import { Icon } from "shared/components/layout";
import { PRIVATE_SCREENS } from "shared/navigation/screen-names";
import { PrivateParamList } from "shared/navigation/types";
const CartInactiveIco = require('shared/assets/navigation/cart-inactive.png');

export const HeaderShoppingCartButton = () => {
   const { navigate } = useNavigation<PrivateParamList>();
   return (
      <TouchableOpacity
         onPress={() => navigate(PRIVATE_SCREENS.ShoppingCartScreen)}
      >
         <Icon source={CartInactiveIco} size={22} />
      </TouchableOpacity>
   );
};
