import { FC } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Divider, Icon } from 'shared/components/layout';
import { Title } from 'shared/components/text';
import { Product } from 'shared/core/domain/entities/product';
import { styles } from './product-card-styles';
import { QuantityCounter } from './quantity-counter';
const CloseIco = require('shared/assets/navigation/close-filter-ico.png');

type ProductCardProps = {
   product: Product;
   //  quantity: number;
   // setProductQuantity(value: boolean): void;
};

export const ProductCard: FC<ProductCardProps> = ({
   product,
   //quantity,
   // setProductQuantity,
}) => {
   
   return (
      <View style={styles.card}>
         <Image style={styles.image} source={product.img} />
         <TouchableOpacity style={styles.remove} onPress={() => {}}>
            <Icon source={CloseIco} size={20} />
         </TouchableOpacity>
         <Divider
            direction='horizontal'
            color='secondary'
            marginHorizontal={10}
         />
         <View>
            <Title theme='light' level='h2'>
               {product.name}
            </Title>
            <Text style={styles.text}>${product.price}</Text>
         </View>
         <QuantityCounter
            price={product.price}
            isAvailable={product.isAvailable}
         />
      </View>
   );
};

export default ProductCard;
