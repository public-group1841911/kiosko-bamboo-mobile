import { StyleSheet } from "react-native";
import { theme } from "shared/styles/theme";

export const styles = StyleSheet.create({
   /** Product card */

   card: {
      paddingVertical: 10,
      flexDirection: 'row',
      backgroundColor: theme.colors.surface,
      borderRadius: 5,
      borderBottomRightRadius: 25,
      borderTopLeftRadius: 25,
      ...theme.boxShadow,
   },
   image: {
      margin: 10,
      height: 100,
      width: 100,
      resizeMode: 'contain',
   },
   text: {

      color: theme.colors.secondary,
   },
   remove: {
      position: 'absolute',
      top: 10,
      right: 10
   },
   /** Product card counter component */
   quantityContainer: {},
   quantityControllers: {},
   quantityButton: {},
   quantitySymbol: {},
   quantity: {},
   price: {},
});
