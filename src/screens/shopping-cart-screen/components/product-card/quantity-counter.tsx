import { FC, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { theme } from 'shared/styles/theme';

type QuantityCounterProps = {
   isAvailable: boolean;
   price: number;
   // quantity: number;
   //  setProductQuantity(value: boolean): void;
};

export const QuantityCounter: FC<QuantityCounterProps> = ({
   isAvailable,
   price,
}) => {
   const [quantity, setQuantity] = useState(1);
   const setProductQuantity = (isAdd: boolean) => {
      if (quantity === 1 && !isAdd) return;
      setQuantity(currentQty => (isAdd ? currentQty + 1 : currentQty - 1));
   };
   return (
      <View style={styles.quantityContainer}>
         <Text style={[styles.price]}>
            ${quantity > 1 ? price * quantity : price}
         </Text>
         <View style={[styles.quantityControllers]}>
            <TouchableOpacity
               style={styles.quantityButtonL}
               onPress={() => setProductQuantity(false)}
            >
               <Text style={styles.quantitySymbol}>-</Text>
            </TouchableOpacity>
            <View style={styles.quantity}>
               <Text>{quantity}</Text>
            </View>
            <TouchableOpacity
               style={styles.quantityButtonR}
               onPress={() => setProductQuantity(true)}
            >
               <Text style={styles.quantitySymbol}>+</Text>
            </TouchableOpacity>
         </View>
      </View>
   );
};

const styles = StyleSheet.create({
   quantityContainer: {
      flexDirection: 'row',
      position: 'absolute',
      bottom: 0,
      right: 0,
      justifyContent: 'space-between',
     
   },
   quantityControllers: {
      flexDirection: 'row',
      borderWidth: 1,
      borderColor: theme.colors.secondary,
      borderBottomRightRadius: 25,
      borderTopLeftRadius: 25,
   },
   quantityButtonL: {
      height: 40,
      width: 38,
      alignItems: 'center',
      justifyContent: 'center',
   },
   quantityButtonR: {
      height: 40,
      width: 38,
      alignItems: 'center',
      justifyContent: 'center',
   },
   quantitySymbol: { fontSize: 18, color: theme.colors.secondary },
   quantity: {
      height: 40,
      width: 40,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.colors.secondary,
   },
   price: {
      fontSize: 20,
      color: theme.colors.primary,
   },
});
