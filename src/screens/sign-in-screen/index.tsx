import { FC } from 'react';
import { KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import { Layout } from 'shared/components/layout';
import { theme } from 'shared/styles/theme';
import { SignInForm } from './sign-in-form';
import { BambooMotifs } from 'shared/components/layout/bamboo-motifs';

const { colors } = theme;

export const SignInScreen: FC = () => {
   return (
      <KeyboardAvoidingView
         style={{ flex: 1 }}
         behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
         <Layout {...styles.layout}>
            <SignInForm />
            <BambooMotifs/>
         </Layout>
      </KeyboardAvoidingView>
   );
};

const styles = StyleSheet.create({
   layout: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: colors.primary,
      paddingHorizontal: 20,
   },
});
