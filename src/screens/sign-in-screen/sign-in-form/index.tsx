import { useNavigation } from '@react-navigation/native';
import { LeafButton } from 'components/buttons';
import { TextField } from 'components/inputs';
import { Logo } from 'components/layout';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { PUBLIC_SCREENS } from 'shared/navigation/screen-names';
import { PublicParamList } from 'shared/navigation/types';
import { theme } from 'shared/styles/theme';
const EmailIco = require('shared/assets/forms/email.png');
const PassIco = require('shared/assets/forms/lock.png');
const { colors } = theme;

type FormData = {
   email: string;
   password: string;
};

export const SignInForm: FC = () => {
   const navigation = useNavigation<PublicParamList>();
   const { control, handleSubmit } = useForm<FormData>();

   const onSignIn = () => null;
   return (
      <View {...styles.form}>
         <Logo />
         <TextField
            control={control}
            label='Correo'
            ico={EmailIco}
            placeholder='comercio@mail.com'
            theme='light'
            keyboardType='email-address'
            name='email'
         />
         <TextField
            control={control}
            label='Contraseña'
            placeholder='*******'
            secure
            ico={PassIco}
            sizeIco={21}
            theme='light'
            name='password'
         />

         <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => navigation.navigate(PUBLIC_SCREENS.AccountRecoveryScreen)}>
            <Text style={styles.forgotPass}>¿Olvidó su contraseña?</Text>
         </TouchableOpacity>

         <LeafButton text='Ingresar' theme='light' onPress={onSignIn} />

         <View style={styles.noAccountContainer}>
            <Text style={styles.noAccountText}>¿No tienes cuenta aun?</Text>
            <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate(PUBLIC_SCREENS.SignUpScreen)}>
               <Text style={styles.register}> Registrarse.</Text>
            </TouchableOpacity>
         </View>
      </View>
   );
};

const styles = StyleSheet.create({
   form: {
      paddingHorizontal: 20,
      borderTopRightRadius: 50
   },
   forgotPass: {
      alignSelf: 'center',
      fontSize: 16,
      paddingTop: 5,
      paddingBottom: 15,
      color: colors.secondary
   },
   noAccountContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 10,
      padding: 20,
      justifyContent: 'space-between'
   },
   noAccountText: {
      fontSize: 16,
      color: colors.secondary
   },
   register: {
      padding: 5,
      fontSize: 15,
      color: colors.tertiary
   }
});
