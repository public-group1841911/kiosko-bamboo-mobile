import { Layout } from 'shared/components/layout';
import { Title } from 'shared/components/text';

export const OrdersScreen = () => {
   return (
      <Layout>
         <Title theme='light'>Pedidos</Title>
      </Layout>
   );
};
