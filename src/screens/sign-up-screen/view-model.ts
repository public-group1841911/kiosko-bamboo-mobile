import { Keyboard } from 'react-native';
import { container } from 'shared/dependencies';
import { AccountCreator } from './core/application/account-creator';
import { useFetchQuestions } from './hooks/useFetchQuestions';
import { AccountFormData } from './sign-up-account-form/types';
const accountCreator: AccountCreator = container.resolve('accountCreator');

export function useViewModel() {
   const { data: questions, error, isError } = useFetchQuestions();

   const onSignUp = (data: Omit<AccountFormData, 'confirmPassword'>) => {
      Keyboard.dismiss();
      console.log({ data });
      accountCreator.run(data);
   };

   return {
      questions: questions ?? [],
      onSignUp
   };
}
