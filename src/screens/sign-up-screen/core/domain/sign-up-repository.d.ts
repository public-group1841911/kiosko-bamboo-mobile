export interface SignUpRepository {
   save(props: UserAccountProps): Promise<void>;
}
