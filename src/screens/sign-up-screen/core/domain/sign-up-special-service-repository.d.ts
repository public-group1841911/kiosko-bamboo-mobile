import { Question } from "./entities/types/question";

export interface SignUpSpecialServiceRepository {
   searchAllQuestions(): Promise<Question[]>;
}
