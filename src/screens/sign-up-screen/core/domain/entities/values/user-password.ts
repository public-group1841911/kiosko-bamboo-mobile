import { StringValueObject } from 'shared/core/domain/entities/values/string-value-object';

export const PASSWORD_MAX_LENGTH = 12;

export class UserPassword extends StringValueObject {
   constructor(value: string) {
      super(value);
   }
}
