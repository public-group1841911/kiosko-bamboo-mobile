import { StringValueObject } from 'shared/core/domain/entities/values/string-value-object';

export class UserAnswer extends StringValueObject {
   constructor(value: string) {
      super(value);
   }
}
