import { StringValueObject } from 'shared/core/domain/entities/values/string-value-object';

export class UserQuestion extends StringValueObject {
   constructor(value: string) {
      super(value);
   }
}
