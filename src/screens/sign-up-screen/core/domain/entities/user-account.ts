import { UserAccountProps } from './types/user-account-props';
import { UserAnswer, UserEmail, UserPassword, UserQuestion } from './values';

export class UserAccount {
   private static _email: UserEmail;
   private static _password: UserPassword;
   private static _question: UserQuestion;
   private static _answer: UserAnswer;

   private constructor({ email, password, question, answer }: UserAccountProps) {
      UserAccount._email = new UserEmail(email);
      UserAccount._password = new UserPassword(password);
      UserAccount._question = new UserQuestion(question);
      UserAccount._answer = new UserAnswer(answer);
   }

   static create(data: UserAccountProps): UserAccount {
      return new UserAccount(data);
   }

   toPrimitives(): UserAccountProps {
      return {
         email: UserAccount._email.value,
         password: UserAccount._password.value,
         question: UserAccount._question.value,
         answer: UserAccount._answer.value
      };
   }
}
