import { HttpAxiosClient } from 'shared/core/infrastructure/http';

type Constructor = { httpAxiosService: HttpAxiosClient };

export class SignUpAxiosService {
   private readonly http: HttpAxiosClient;

   constructor({ httpAxiosService }: Constructor) {
      this.http = httpAxiosService;
   }

   async saveAccount(props: any) {
      console.log(props, ' from service.');
      // this.http.post(props).then();
   }
}
