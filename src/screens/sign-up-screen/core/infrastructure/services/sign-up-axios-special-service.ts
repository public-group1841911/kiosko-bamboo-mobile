import { HttpAxiosClient } from 'shared/core/infrastructure/http';
import { AxiosResp } from 'shared/core/infrastructure/http/types/axios-http-types';
import { Question } from '../../domain/entities/types/question';
import { questionsQuery } from '../graphql-queries';
import { QuestionsGraphqlReq } from '../graphql-queries/types';

type Constructor = { httpAxiosService: HttpAxiosClient };

export class SignUpAxiosSpecialService {
   private readonly http: HttpAxiosClient;

   constructor({ httpAxiosService }: Constructor) {
      this.http = httpAxiosService;
   }

   async getAllQuestions(): Promise<Question[]> {
      return await this.http
         .get<AxiosResp<QuestionsGraphqlReq>>({ body: questionsQuery })
         .then(({ data: { data } }) => data.searchQuestions)
         .catch(err => err);
   }
}
