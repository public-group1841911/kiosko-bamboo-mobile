export type QuestionsGraphqlReq = { searchQuestions: { id: number; question: string }[] };
