import { SignUpRepository } from "../domain/sign-up-repository";
import { SignUpAxiosService } from "./services/sign-up-axios-service";

type Constructor = { signUpAxiosService: SignUpAxiosService };

export class SignUpAxiosRepository implements SignUpRepository {
   private readonly service: SignUpAxiosService;
   
   constructor({ signUpAxiosService }: Constructor) {
      this.service = signUpAxiosService
   }

   async save(props: any): Promise<void> {
      return this.service.saveAccount(props)
   }
}