import { Question } from "../domain/entities/types/question";
import { SignUpSpecialServiceRepository } from "../domain/sign-up-special-service-repository";
import { SignUpAxiosSpecialService } from "./services/sign-up-axios-special-service";

type Constructor = { signUpAxiosSpecialService: SignUpAxiosSpecialService };

export class SignUpAxiosSpecialServiceRepository implements SignUpSpecialServiceRepository {
   private readonly service: SignUpAxiosSpecialService;

   constructor({ signUpAxiosSpecialService }: Constructor) {
      this.service = signUpAxiosSpecialService;
   }

   async searchAllQuestions(): Promise<Question[]> {
      return await this.service.getAllQuestions()
   }
}
