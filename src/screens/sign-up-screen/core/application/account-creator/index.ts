import { UserAccountProps } from '../../domain/entities/types/user-account-props';
import { UserAccount } from '../../domain/entities/user-account';
import { SignUpRepository } from '../../domain/sign-up-repository';

type Constructor = {
   signUpRepository: SignUpRepository;
};

export class AccountCreator {
   private readonly repository: SignUpRepository;

   constructor({ signUpRepository }: Constructor) {
      this.repository = signUpRepository;
   }

   async run(data: UserAccountProps): Promise<void> {
      const account = UserAccount.create(data);
      this.repository.save(account);
   }
}
