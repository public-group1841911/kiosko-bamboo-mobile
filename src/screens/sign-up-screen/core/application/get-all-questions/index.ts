import { SignUpSpecialServiceRepository } from '../../domain/sign-up-special-service-repository';

type Constructor = { signUpAxiosSpecialServiceRepository: SignUpSpecialServiceRepository };

export class GetAllQuestions {
   private readonly repository: SignUpSpecialServiceRepository;
   constructor({ signUpAxiosSpecialServiceRepository }: Constructor) {
      this.repository = signUpAxiosSpecialServiceRepository;
   }

   async run() {
      return await this.repository.searchAllQuestions();
   }
}
