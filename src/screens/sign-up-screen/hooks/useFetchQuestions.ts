import { container } from 'shared/dependencies';
import { useFetch } from 'shared/hooks/user-fetch';
import { GetAllQuestions } from '../core/application/get-all-questions';
import { Question } from '../core/domain/entities/types/question';
const getAllQuestions: GetAllQuestions = container.resolve('getAllQuestions');

export function useFetchQuestions() {
   return useFetch<Question[], Error>('security-questions', () => getAllQuestions.run());
}
