import { FC } from 'react';
import { KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import { BackButton } from 'shared/components/buttons';
import { Layout } from 'shared/components/layout';
import { BambooMotifs } from 'shared/components/layout/bamboo-motifs';
import { PublicNavigationProps } from 'shared/navigation/types';
import { theme } from 'shared/styles/theme';
import { SignUpAccountForm } from './sign-up-account-form';

const { colors } = theme;

export const SignUpAccountScreen: FC<PublicNavigationProps> = ({ navigation }) => {
   return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
         <Layout {...styles.layout}>
            <BackButton goBack={() => navigation.goBack()} />
            <SignUpAccountForm />
            <BambooMotifs />
         </Layout>
      </KeyboardAvoidingView>
   );
};

const styles = StyleSheet.create({
   layout: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: colors.primary,
      paddingHorizontal: 20
   }
});
