import { LeafButton } from 'components/buttons';
import { SelectField, TextField } from 'components/inputs';
import { StyleSheet, View } from 'react-native';
import { Title } from 'shared/components/text';
import { formResolver, useForm } from 'shared/hooks/use-form';
import { theme } from 'shared/styles/theme';
import { useViewModel } from '../view-model';
import { AccountFormData } from './types';
import { signUpAccountSchema } from './validation';
const EmailIco = require('shared/assets/forms/email.png');
const PassIco = require('shared/assets/forms/lock.png');
const { colors } = theme;

export const SignUpAccountForm = () => {
   const { questions, onSignUp } = useViewModel();
   const { control, handleSubmit } = useForm<AccountFormData>({
      resolver: formResolver(signUpAccountSchema),
      mode: 'all'
   });

   return (
      <View {...styles.form}>
         <Title>Registrarse</Title>
         <TextField
            name='email'
            label='Correo'
            ico={EmailIco}
            placeholder='comercio@mail.com'
            theme='light'
            keyboardType='email-address'
            control={control}
         />
         <TextField
            name='password'
            label='Contraseña'
            placeholder='*******'
            secure
            ico={PassIco}
            sizeIco={21}
            theme='light'
            control={control}
         />
         <TextField
            name='confirmPassword'
            label='Confirmar contraseña'
            placeholder='*******'
            secure
            ico={PassIco}
            sizeIco={21}
            theme='light'
            control={control}
         />
         <SelectField
            name='question'
            data={questions}
            valueField='id'
            labelField='question'
            label='Pregunta de seguridad'
            placeholder='Seleccionar'
            theme='light'
            control={control}
         />
         <TextField name='answer' label='Respuesta' placeholder='Escribir' theme='light' control={control} />
         <LeafButton text='Enviar' theme='light' onPress={handleSubmit(onSignUp)} />
      </View>
   );
};

const styles = StyleSheet.create({
   form: {
      paddingHorizontal: 20,
      borderTopRightRadius: 50
   },
   forgotPass: {
      alignSelf: 'center',
      fontSize: 16,
      paddingTop: 5,
      paddingBottom: 30,
      color: colors.secondary
   }
});
