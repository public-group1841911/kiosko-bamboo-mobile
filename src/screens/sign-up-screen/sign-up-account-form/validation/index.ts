import z from 'zod';

const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const minPasswordLength = 8;

export const signUpAccountSchema = z
   .object({
      email: z
         .string()
         .min(1, { message: 'Campo de correo requerido' })
         .email({ message: 'Formato de correo invalido.' }),
      password: z
         .string()
         .min(1, { message: 'Campo de contraseña requerido' })
         .min(8, { message: 'Debe cumplir un mínimo de 8 caracteres' }),
      confirmPassword: z
         .string()
         .min(1, { message: 'Debe confirmar la contraseña.' })
         .min(8, { message: 'Debe cumplir un mínimo de 8 caracteres' }),
      question: z
         .object({})
         .nullable()
         .refine(data => data !== null, { message: 'Campo de pregunta requerido.' }),
      answer: z.string().min(1, { message: 'Campo de respuesta requerido.' })
   })
   .refine(data => data.password === data.confirmPassword, {
      message: 'Las contraseñas no coinciden.',
      path: ['confirmPassword']
   });
