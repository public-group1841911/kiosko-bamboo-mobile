export type AccountFormData = {
   email: string;
   password: string;
   confirmPassword: string;
   question: string;
   answer: string;
};
