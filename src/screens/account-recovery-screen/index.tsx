import { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { BackButton, LeafButton } from 'shared/components/buttons';
import { TextField } from 'shared/components/inputs';
import { Box, Layout } from 'shared/components/layout';
import { BambooMotifs } from 'shared/components/layout/bamboo-motifs';
import { Title } from 'shared/components/text';
import { Paragraph } from 'shared/components/text/paragraph';
import { useForm } from 'shared/hooks/use-form';
import { PublicNavigationProps } from 'shared/navigation/types';
import { theme } from 'shared/styles/theme';
const EmailIco = require('shared/assets/forms/email.png');

export const AccountRecoveryScreen: FC<PublicNavigationProps> = ({ navigation }) => {
   const { control, handleSubmit } = useForm({
      mode: 'all'
   });

   const onRecovery = () => null;

   return (
      <Layout {...styles.layout}>
         <Box position='absolute' top={70} left={20} backgroundColor='transparent'  >
            <BackButton goBack={() => navigation.goBack()} />
         </Box>
         <Title>Recuperar cuenta</Title>
         <Paragraph>
            Coloca la dirección de correo asociado a tu cuenta y te enviaremos un enlace para restablecer la contraseña.
         </Paragraph>

         <TextField
            name='email'
            label='Correo'
            ico={EmailIco}
            placeholder='comercio@mail.com'
            theme='light'
            keyboardType='email-address'
            control={control}
         />

         <LeafButton text='Enviar' theme='light' onPress={handleSubmit(onRecovery)} />
         <BambooMotifs />
      </Layout>
   );
};

const styles = StyleSheet.create({
   layout: {
      flex:1,
      justifyContent: 'center',
      backgroundColor: theme.colors.primary,
      paddingHorizontal: 40
   }
});
