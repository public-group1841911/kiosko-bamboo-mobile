import { FC } from 'react';
import { Image, ImageSourcePropType, TouchableOpacity } from 'react-native';
import { Title } from 'shared/components/text';
import { styles } from './category-card-styles';

type CategoryCardProps = {
   name: string;
   img: ImageSourcePropType;
   position: object;
   onPress(): void;
};

export const CategoryCard: FC<CategoryCardProps> = ({
   name,
   img,
   position,
   onPress
}) => {
   return (
      <TouchableOpacity style={{ ...styles.card }} activeOpacity={0.6} onPress={onPress}>
         <Title style={styles.title} level='h3'>
            {name}
         </Title>
         <Image style={{ ...styles.image, ...position }} source={img} />
      </TouchableOpacity>
   );
};
