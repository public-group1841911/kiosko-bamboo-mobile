import { StyleSheet } from "react-native";
import { theme } from "shared/styles/theme";

export const styles = StyleSheet.create({
   card: {
      borderRadius: 8,
      height: 120,
      backgroundColor: '#c4ceb7',
      overflow: 'hidden',
      ...theme.boxShadow,
   },
   title: {
      paddingLeft: 10,
      zIndex: 1,
      color: theme.colors.secondary,
   },
   image: {
      resizeMode: 'contain',
   },
});