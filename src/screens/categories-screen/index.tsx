import { ScrollView, StyleSheet } from 'react-native';
import { GridView, Layout } from 'shared/components/layout';
import { Title } from 'shared/components/text';
import { metrics } from 'shared/styles/metrics';
import { theme } from 'shared/styles/theme';
import { CategoryCard } from './category-card';
import { categories } from './data';
import { useNavigation } from '@react-navigation/native';
import { PrivateParamList } from 'shared/navigation/types';

export const CategoriesScreen = () => {
   const { navigate } = useNavigation<PrivateParamList>();
   return (
      <Layout paddingTop={metrics.headerHeight} paddingHorizontal={5}>
         <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
            <Title style={styles.categoryTitle} theme='light' level='h2'>
               Categorías
            </Title>
            <GridView
               col={2}
               colGap={8}
               rowGap={15}
               data={categories}
               renderItem={category => <CategoryCard {...category} onPress={() => navigate('ProductsScreen')}/>}
            />
         </ScrollView>
      </Layout>
   );
};

const styles = StyleSheet.create({
   categoryTitle: {
      marginVertical: 5,
      marginLeft: 10,
      color: theme.colors.secondary,
   },
});
