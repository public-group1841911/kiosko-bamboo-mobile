const BakeryImg = require('shared/assets/categories/images/bakery-img.png');
const BeansImg = require('shared/assets/categories/images/beans-img.png');
const FrozenImg = require('shared/assets/categories/images/frozen-food-img.png');
const MeatImg = require('shared/assets/categories/images/meat-img.png');
const VeggiesImg = require('shared/assets/categories/images/veggies-img.png');
const FruitsImg = require('shared/assets/categories/images/fruits-img.png');
const CerealsImg = require('shared/assets/categories/images/cereals-img.png');
const DairyImg = require('shared/assets/categories/images/dairy-img.png');
const PantryImg = require('shared/assets/categories/images/pantry-img.png');
const SeafoodImg = require('shared/assets/categories/images/seafood-img.png');
const SausagesImg = require('shared/assets/categories/images/sausages-img.png');
const SweetsImg = require('shared/assets/categories/images/sweets-img.png');
const CandiesImg = require('shared/assets/categories/images/candies-img.png');
const SnacksImg = require('shared/assets/categories/images/snacks-img.png');
const DrinksImg = require('shared/assets/categories/images/drinks-img.png');
const PersonalCareImg = require('shared/assets/categories/images/personal-care-img.png');
const CosmeticsImg = require('shared/assets/categories/images/cosmetics-img.png');
const GlutenFreeImg = require('shared/assets/categories/images/gluten-free-img.png');
const AnimalsImg = require('shared/assets/categories/images/animals-img.png');
const CleaningImg = require('shared/assets/categories/images/cleaning-img.png');

export const categories = [
   {
      name: 'Bebidas',
      img: DrinksImg,
      sub: [
         { id: 1, name: 'Energéticas' },
         { id: 2, name: 'Gaseosas' },
         { id: 3, name: 'Jugos' },
      ],
      position: {
         height: 100,
         bottom: 15,
         left: 15,
      },
   },
   {
      name: 'Cárnicos',
      img: MeatImg,
      sub: [
         { id: 1, name: 'Cerdo' },
         { id: 2, name: 'Pollo' },
         { id: 3, name: 'Vacuno' },
      ],
      position: {
         height: 105,
         bottom: 0,
         right: 60,
      },
   },
   {
      name: 'Cereales',
      img: CerealsImg,
      sub: [],
      position: {
         height: 120,
         bottom: 20,
      },
   },
   {
      name: 'Confites',
      img: CandiesImg,
      sub: [
         { id: 1, name: 'Caramelos' },
         { id: 2, name: 'Chocolate' },
         { id: 3, name: 'Galletas' },
      ],
      position: {
         height: 125,
         bottom: 30,
         right: 10,
      },
   },
   {
      name: 'Congelados',
      img: FrozenImg,
      sub: [
         { id: 1, name: 'Comida rápida' },
         { id: 2, name: 'Helados' },
         { id: 3, name: 'Marisquería y pescado' },
         { id: 4, name: 'Tubérculos' },
         { id: 5, name: 'Verduras' },
      ],
      position: {
         height: 75,
         bottom: 0,
         right: 105,
      },
   },
   {
      name: 'Despensa',
      img: PantryImg,
      sub: [
         { id: 1, name: 'Aceite oliva' },
         { id: 2, name: 'Aceite vegetal' },
         { id: 3, name: 'Azúcar' },
         { id: 4, name: 'Café' },
         { id: 5, name: 'Dips' },
         { id: 6, name: 'Encurtidos' },
         { id: 7, name: 'Enlatados' },
         { id: 8, name: 'Especias' },
         { id: 9, name: 'Fermentos' },
         { id: 10, name: 'Sal' },
         { id: 11, name: 'Salsas y aderezos' },
         { id: 12, name: 'Vinagre' },
      ],
      position: {
         height: 100,
         bottom: 20,
         left: 25,
      },
   },
   {
      name: 'Dulces - Pastelería',
      img: SweetsImg,
      sub: [],
      position: {
         height: 85,
         bottom: 5,
         left: 5,
      },
   },
   {
      name: 'Embutidos - deli',
      img: SausagesImg,
      sub: [
         { id: 1, name: 'Chorizos' },
         { id: 2, name: 'Jamón' },
         { id: 3, name: 'Mortadela' },
         { id: 5, name: 'Pasteles de carne' },
         { id: 6, name: 'Salchichas' },
         { id: 7, name: 'Tocino' },
      ],
      position: {
         height: 80,
         bottom: 0,
         right: 70,
      },
   },
   {
      name: 'Frutas',
      img: FruitsImg,
      sub: [
         { id: 1, name: 'Frutos secos' },
         { id: 2, name: 'Dátiles' },
         { id: 3, name: 'Orgánicos frescos' },
      ],
      position: {
         height: 120,
         bottom: 30,
         left: 15,
      },
   },
   {
      name: 'Lácteos y huevos',
      img: DairyImg,
      sub: [
         { id: 1, name: 'Huevos' },
         { id: 2, name: 'Quesos' },
         { id: 3, name: 'Leche' },
      ],
      position: {
         height: 110,
         bottom: 20,
         left: 10,
      },
   },
   {
      name: 'Legumbres',
      img: BeansImg,
      sub: [
         { id: 1, name: 'Arvejas' },
         { id: 2, name: 'Caraotas' },
         { id: 3, name: 'Frijoles' },
         { id: 4, name: 'Lentejas' },
      ],
      position: {
         height: 140,
         bottom: 25,
         left: 25,
      },
   },
   {
      name: 'Panadería',
      img: BakeryImg,
      sub: [],
      position: {
         height: 90,
         bottom: 0,
         left: 20,
      },
   },
   {
      name: 'Verduras',
      img: VeggiesImg,
      sub: [],
      position: {
         height: 100,
         bottom: 20,
         left: 15,
      },
   },
   {
      name: 'Pescado y marisquería',
      img: SeafoodImg,
      sub: [
         { id: 1, name: 'Pescado' },
         { id: 2, name: 'Marisquería' },
      ],
      position: {
         height: 150,
         bottom: 30,
         right: 10,
      },
   },
   {
      name: 'Snacks',
      img: SnacksImg,
      sub: [],
      position: {
         height: 85,
         bottom: 0,
         right: 50,
      },
   },
   {
      name: 'Cuidado personal',
      img: PersonalCareImg,
      sub: [
         { id: 1, name: 'Champú' },
         { id: 2, name: 'Capillos' },
         { id: 3, name: 'Colonias' },
         { id: 4, name: 'Cremas' },
         { id: 5, name: 'Jabón' },
         { id: 6, name: 'Rasuradoras' },
         { id: 7, name: 'Otros' },
      ],
      position: {
         height: 110,
         bottom: 15,
         left: 20,
      },
   },
   {
      name: 'Salud y belleza',
      img: CosmeticsImg,
      sub: [],
      position: {
         height: 90,
         bottom: 10,
         left: 20,
      },
   },
   {
      name: 'Libre de gluten',
      img: GlutenFreeImg,
      sub: [],
      position: {
         height: 80,
         bottom: 0,
         left: 35,
      },
   },
   {
      name: 'Productos de limpieza',
      img: CleaningImg,
      sub: [
         { id: 1, name: 'Cloro' },
         { id: 2, name: 'Cepillos' },
         { id: 3, name: 'Detergentes' },
         { id: 4, name: 'Esponjas' },
         { id: 5, name: 'Guantes' },
         { id: 6, name: 'Haragán' },
         { id: 7, name: 'Lavaplatos' },
         { id: 8, name: 'Limpia cerámicas' },
         { id: 9, name: 'Quita grasas' },
         { id: 10, name: 'Quita manchas' },
         { id: 11, name: 'Suavizantes' },
         { id: 12, name: 'Otros' },
      ],
      position: {
         height: 80,
         bottom: 0,
         right: 90,
      },
   },
   {
      name: 'Animales',
      img: AnimalsImg,
      sub: [
         { id: 1, name: 'Conejos' },
         { id: 2, name: 'Gatos' },
         { id: 3, name: 'Detergentes' },
         { id: 4, name: 'Esponjas' },
      ],
      position: {
         height: 95,
         bottom: 5,
         left: 10,
      },
   },
];
