import { Layout } from 'shared/components/layout';
import { Title } from 'shared/components/text';

export const FavoritesScreen = () => {
   return (
      <Layout>
         <Title theme='light'>Favoritos</Title>
      </Layout>
   );
};
